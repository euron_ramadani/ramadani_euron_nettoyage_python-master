# routes_gestion_personne_equipe.py
# OM 2020.04.16 Gestions des "routes" FLASK pour la table intermédiaire qui associe les personne et les equipe.

from flask import render_template, request, flash, session
from APP_NETTOYAGE import obj_mon_application
from APP_NETTOYAGE.equipe.data_gestion_equipe import Gestionequipe
from APP_NETTOYAGE.personne_equipe.data_gestion_personne_equipe import Gestionpersonneequipe


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /personne_equipe_afficher_concat
# Récupère la liste de tous les personne et de tous les equipe associés aux personne.
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/personne_equipe_afficher_concat/<int:id_personne_sel>", methods=['GET', 'POST'])
def personne_equipe_afficher_concat (id_personne_sel):
    print("id_personne_sel ", id_personne_sel)
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_equipe = Gestionpersonneequipe()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionequipe()
            # Fichier data_gestion_equipe.py
            data_personne_equipe_afficher_concat = obj_actions_equipe.personne_equipe_afficher_data_concat(id_personne_sel)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data equipe", data_personne_equipe_afficher_concat, "type ", type(data_personne_equipe_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_personne_equipe_afficher_concat:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données equipe affichés dans equipepersonne!!", "success")
            else:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_personne_equipe" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("personne_equipe/personne_equipe_afficher.html",
                           data=data_personne_equipe_afficher_concat)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.21 Définition d'une "route" /gf_edit_personne_equipe_selected
# Récupère la liste de tous les equipe du personne sélectionné.
# Nécessaire pour afficher tous les "TAGS" des equipe, ainsi l'utilisateur voit les equipe à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_edit_personne_equipe_selected", methods=['GET', 'POST'])
def gf_edit_personne_equipe_selected ():
    if request.method == "GET":
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_equipe = Gestionequipe()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionequipe()
            # Fichier data_gestion_equipe.py
            # Pour savoir si la table "t_equipe" est vide, ainsi on empêche l’affichage des tags
            # dans le render_template(personne_equipe_modifier_tags_dropbox.html)
            data_equipe_all = obj_actions_equipe.equipe_afficher_data()

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données de la table intermédiaire.
            obj_actions_equipe = Gestionpersonneequipe()

            # OM 2020.04.21 Récupère la valeur de "id_personne" du formulaire html "personne_equipe_afficher.html"
            # l'utilisateur clique sur le lien "Modifier equipe de ce personne" et on récupère la valeur de "id_personne" grâce à la variable "id_personne_equipe_edit_html"
            # <a href="{{ url_for('gf_edit_personne_equipe_selected', id_personne_equipe_edit_html=row.id_personne) }}">Modifier les equipe de ce personne</a>
            id_personne_equipe_edit = request.values['id_personne_equipe_edit_html']

            # OM 2020.04.21 Mémorise l'id du personne dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_personne_equipe_edit'] = id_personne_equipe_edit

            # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
            valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_personne_equipe_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la classe Gestionpersonneequipe()
            # 1) Sélection du personne choisi
            # 2) Sélection des equipe "déjà" attribués pour le personne.
            # 3) Sélection des equipe "pas encore" attribués pour le personne choisi.
            # Fichier data_gestion_personne_equipe.py
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "personne_equipe_afficher_data"
            data_personne_equipe_selected, data_personne_equipe_non_attribues, data_personne_equipe_attribues = \
                obj_actions_equipe.personne_equipe_afficher_data(valeur_id_personne_selected_dictionnaire)

            lst_data_personne_selected = [item['id_personne'] for item in data_personne_equipe_selected]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_selected  ", lst_data_personne_selected,
                  type(lst_data_personne_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les equipe qui ne sont pas encore sélectionnés.
            lst_data_personne_equipe_non_attribues = [item['id_equipe'] for item in data_personne_equipe_non_attribues]
            session['session_lst_data_personne_equipe_non_attribues'] = lst_data_personne_equipe_non_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_equipe_non_attribues  ", lst_data_personne_equipe_non_attribues,
                  type(lst_data_personne_equipe_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les equipe qui sont déjà sélectionnés.
            lst_data_personne_equipe_old_attribues = [item['id_equipe'] for item in data_personne_equipe_attribues]
            session['session_lst_data_personne_equipe_old_attribues'] = lst_data_personne_equipe_old_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_equipe_old_attribues  ", lst_data_personne_equipe_old_attribues,
                  type(lst_data_personne_equipe_old_attribues))

            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data data_personne_equipe_selected", data_personne_equipe_selected, "type ", type(data_personne_equipe_selected))
            print(" data data_personne_equipe_non_attribues ", data_personne_equipe_non_attribues, "type ",
                  type(data_personne_equipe_non_attribues))
            print(" data_personne_equipe_attribues ", data_personne_equipe_attribues, "type ",
                  type(data_personne_equipe_attribues))

            # Extrait les valeurs contenues dans la table "t_equipe", colonne "nom_equipe"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_equipe
            lst_data_personne_equipe_non_attribues = [item['nom_equipe'] for item in data_personne_equipe_non_attribues]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_all_equipe gf_edit_personne_equipe_selected ", lst_data_personne_equipe_non_attribues,
                  type(lst_data_personne_equipe_non_attribues))

            # Différencier les messages si la table est vide.
            if lst_data_personne_selected == [None]:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_personne_equipe" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données equipe affichées dans equipepersonne!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("personne_equipe/personne_equipe_modifier_tags_dropbox.html",
                           data_equipe=data_equipe_all,
                           data_personne_selected=data_personne_equipe_selected,
                           data_equipe_attribues=data_personne_equipe_attribues,
                           data_equipe_non_attribues=data_personne_equipe_non_attribues)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /gf_update_personne_equipe_selected
# Récupère la liste de tous les equipe du personne sélectionné.
# Nécessaire pour afficher tous les "TAGS" des equipe, ainsi l'utilisateur voit les equipe à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_update_personne_equipe_selected", methods=['GET', 'POST'])
def gf_update_personne_equipe_selected ():
    if request.method == "POST":
        try:
            # Récupère l'id du personne sélectionné
            id_personne_selected = session['session_id_personne_equipe_edit']
            print("session['session_id_personne_equipe_edit'] ", session['session_id_personne_equipe_edit'])

            # Récupère la liste des equipe qui ne sont pas associés au personne sélectionné.
            old_lst_data_personne_equipe_non_attribues = session['session_lst_data_personne_equipe_non_attribues']
            print("old_lst_data_personne_equipe_non_attribues ", old_lst_data_personne_equipe_non_attribues)

            # Récupère la liste des equipe qui sont associés au personne sélectionné.
            old_lst_data_personne_equipe_attribues = session['session_lst_data_personne_equipe_old_attribues']
            print("old_lst_data_personne_equipe_old_attribues ", old_lst_data_personne_equipe_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme equipe dans le composant "tags-selector-tagselect"
            # dans le fichier "personne_equipe_modifier_tags_dropbox.html"
            new_lst_str_personne_equipe = request.form.getlist('name_select_tags')
            print("new_lst_str_personne_equipe ", new_lst_str_personne_equipe)

            # OM 2020.04.29 Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_personne_equipe_old = list(map(int, new_lst_str_personne_equipe))
            print("new_lst_personne_equipe ", new_lst_int_personne_equipe_old, "type new_lst_personne_equipe ",
                  type(new_lst_int_personne_equipe_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2020.04.29 Une liste de "id_equipe" qui doivent être effacés de la table intermédiaire "t_personne_equipe".
            lst_diff_equipe_delete_b = list(
                set(old_lst_data_personne_equipe_attribues) - set(new_lst_int_personne_equipe_old))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_equipe_delete_b ", lst_diff_equipe_delete_b)

            # OM 2020.04.29 Une liste de "id_equipe" qui doivent être ajoutés à la BD
            lst_diff_equipe_insert_a = list(
                set(new_lst_int_personne_equipe_old) - set(old_lst_data_personne_equipe_attribues))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_equipe_insert_a ", lst_diff_equipe_insert_a)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_equipe = Gestionpersonneequipe()

            # Pour le personne sélectionné, parcourir la liste des equipe à INSÉRER dans la "t_personne_equipe".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_equipe_ins in lst_diff_equipe_insert_a:
                # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                # et "id_equipe_ins" (l'id du equipe dans la liste) associé à une variable.
                valeurs_personne_sel_equipe_sel_dictionnaire = {"value_FK_personne": id_personne_selected,
                                                           "value_FK_equipe": id_equipe_ins}
                # Insérer une association entre un(des) equipe(s) et le personne sélectionner.
                obj_actions_equipe.personne_equipe_add(valeurs_personne_sel_equipe_sel_dictionnaire)

            # Pour le personne sélectionné, parcourir la liste des equipe à EFFACER dans la "t_personne_equipe".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_equipe_del in lst_diff_equipe_delete_b:
                # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                # et "id_equipe_del" (l'id du equipe dans la liste) associé à une variable.
                valeurs_personne_sel_equipe_sel_dictionnaire = {"value_FK_personne": id_personne_selected,
                                                           "value_FK_equipe": id_equipe_del}
                # Effacer une association entre un(des) equipe(s) et le personne sélectionner.
                obj_actions_equipe.personne_equipe_delete(valeurs_personne_sel_equipe_sel_dictionnaire)

            # Récupère les données grâce à une requête MySql définie dans la classe Gestionequipe()
            # Fichier data_gestion_equipe.py
            # Afficher seulement le personne dont les equipe sont modifiés, ainsi l'utilisateur voit directement
            # les changements qu'il a demandés.
            data_personne_equipe_afficher_concat = obj_actions_equipe.personne_equipe_afficher_data_concat(id_personne_selected)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data equipe", data_personne_equipe_afficher_concat, "type ", type(data_personne_equipe_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_personne_equipe_afficher_concat == None:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_personne_equipe" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données equipe affichées dans equipepersonne!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Après cette mise à jour de la table intermédiaire "t_personne_equipe",
    # on affiche les personne et le(urs) equipe(s) associé(s).
    return render_template("personne_equipe/personne_equipe_afficher.html",
                           data=data_personne_equipe_afficher_concat)
