# data_gestion_personne_equipe.py
# OM 2020.04.22 Permet de gérer (CRUD) les données de la table intermédiaire "t_personne_equipe"

from flask import flash
from APP_NETTOYAGE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_NETTOYAGE.DATABASE.erreurs import *


class Gestionpersonneequipe():
    def __init__ (self):
        try:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print("dans le try de gestions equipe")
            # OM 2020.04.11 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion equipe personne ...terrible erreur, il faut connecter une base de donnée", "danger")
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur Gestionpersonneequipe {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur Gestionpersonneequipe ")

    def equipe_afficher_data (self):
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_equipe"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher
            strsql_equipe_afficher = """SELECT id_equipe, nom_equipe FROM t_equipe ORDER BY id_equipe ASC"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_equipe_afficher)
                # Récupère les données de la requête.
                data_equipe = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_equipe ", data_equipe, " Type : ", type(data_equipe))
                # Retourne les données du "SELECT"
                return data_equipe
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def personne_equipe_afficher_data (self, valeur_id_personne_selected_dict):
        print("valeur_id_personne_selected_dict8..", valeur_id_personne_selected_dict)
        try:

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_equipe"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_personne_selected = """SELECT id_personne, nom_personne, prenom_personne, GROUP_CONCAT(id_equipe) as equipepersonne FROM t_personne_equipe AS T1
                                        INNER JOIN t_personne AS T2 ON T2.id_personne = T1.FK_personne
                                        INNER JOIN t_equipe AS T3 ON T3.id_equipe = T1.FK_equipe
                                        WHERE id_personne = %(value_id_personne_selected)s"""

            strsql_personne_equipe_non_attribues = """SELECT id_equipe, nom_equipe FROM t_equipe
                                                    WHERE id_equipe not in(SELECT id_equipe as idequipepersonne FROM t_personne_equipe AS T1
                                                    INNER JOIN t_personne AS T2 ON T2.id_personne = T1.FK_personne
                                                    INNER JOIN t_equipe AS T3 ON T3.id_equipe = T1.FK_equipe
                                                    WHERE id_personne = %(value_id_personne_selected)s)"""

            strsql_personne_equipe_attribues = """SELECT id_personne, id_equipe, nom_equipe FROM t_personne_equipe AS T1
                                            INNER JOIN t_personne AS T2 ON T2.id_personne = T1.FK_personne
                                            INNER JOIN t_equipe AS T3 ON T3.id_equipe = T1.FK_equipe
                                            WHERE id_personne = %(value_id_personne_selected)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_personne_equipe_non_attribues, valeur_id_personne_selected_dict)
                # Récupère les données de la requête.
                data_personne_equipe_non_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("dfad data_personne_equipe_non_attribues ", data_personne_equipe_non_attribues, " Type : ",
                      type(data_personne_equipe_non_attribues))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_personne_selected, valeur_id_personne_selected_dict)
                # Récupère les données de la requête.
                data_personne_selected = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_personne_selected  ", data_personne_selected, " Type : ", type(data_personne_selected))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_personne_equipe_attribues, valeur_id_personne_selected_dict)
                # Récupère les données de la requête.
                data_personne_equipe_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_personne_equipe_attribues ", data_personne_equipe_attribues, " Type : ",
                      type(data_personne_equipe_attribues))

                # Retourne les données du "SELECT"
                return data_personne_selected, data_personne_equipe_non_attribues, data_personne_equipe_attribues
        except pymysql.Error as erreur:
            print(f"DGGF gfad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def personne_equipe_afficher_data_concat (self, id_personne_selected):
        print("id_personne_selected  ", id_personne_selected)
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_equipe"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_personne_equipe_afficher_data_concat = """SELECT id_personne, nom_personne, prenom_personne,
                                                            GROUP_CONCAT(nom_equipe) as equipepersonne FROM t_personne_equipe AS T1
                                                            RIGHT JOIN t_personne AS T2 ON T2.id_personne = T1.FK_personne
                                                            LEFT JOIN t_equipe AS T3 ON T3.id_equipe = T1.FK_equipe
                                                            GROUP BY id_personne"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # le paramètre 0 permet d'afficher tous les personne
                # Sinon le paramètre représente la valeur de l'id du personne
                if id_personne_selected == 0:
                    mc_afficher.execute(strsql_personne_equipe_afficher_data_concat)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                    valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_personne_selected}
                    strsql_personne_equipe_afficher_data_concat += """ HAVING id_personne= %(value_id_personne_selected)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_personne_equipe_afficher_data_concat, valeur_id_personne_selected_dictionnaire)

                # Récupère les données de la requête.
                data_personne_equipe_afficher_concat = mc_afficher.fetchall()
                # Affichage dans la console
                print("dggf data_personne_equipe_afficher_concat ", data_personne_equipe_afficher_concat, " Type : ",
                      type(data_personne_equipe_afficher_concat))

                # Retourne les données du "SELECT"
                return data_personne_equipe_afficher_concat


        except pymysql.Error as erreur:
            print(f"DGGF gfadc pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfadc Exception {erreur.args}")
            raise MaBdErreurConnexion(
                f"DGG gfadc Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfadc pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def personne_equipe_add (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_personne" et "id_equipe" dans la "t_personne_equipe"
            strsql_insert_personne_equipe = """INSERT INTO t_personne_equipe (id_personne_equipe, FK_equipe, FK_personne)
                                            VALUES (NULL, %(value_FK_equipe)s, %(value_FK_personne)s)"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_personne_equipe, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

    def personne_equipe_delete (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Effacer une (des) association(s) existantes entre "id_personne" et "id_equipe" dans la "t_personne_equipe"
            strsql_delete_personne_equipe = """DELETE FROM t_personne_equipe WHERE FK_equipe = %(value_FK_equipe)s AND FK_personne = %(value_FK_personne)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_delete_personne_equipe, valeurs_insertion_dictionnaire)
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème personne_equipe_delete Gestions equipe personne numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème personne_equipe_delete Gestions equipe personne  numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème personne_equipe_delete Gestions equipe personne  {erreur}")

    def edit_equipe_data (self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le equipe sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_personnes = "SELECT id_equipe, nom_equipe FROM t_equipe WHERE id_equipe = %(value_id_equipe)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_personnes, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_equipe_data Data Gestions equipe numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions equipe numéro de l'erreur : {erreur}", "danger")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_equipe_data d'un equipe Data Gestions equipe {erreur}")

    def update_equipe_data (self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditnomequipeHTML" du form HTML "equipeEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnomequipeHTML" value="{{ row.nom_equipe }}"/></td>
            str_sql_update_nomequipe = "UPDATE t_equipe SET nom_equipe = %(value_name_genre)s WHERE id_equipe = %(value_id_equipe)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_nomequipe, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_equipe_data Data Gestions equipe numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions equipe numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_equipe_data d\'un equipe Data Gestions equipe {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "warning")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash(f"Doublon !!! Introduire une valeur différente", "warning")
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_equipe_data Data Gestions equipe numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_equipe_data d'un equipe DataGestionsequipe {erreur}")

    def delete_select_equipe_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditnomequipeHTML" du form HTML "equipeEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnomequipeHTML" value="{{ row.nom_equipe }}"/></td>

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le equipe sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_equipe = "SELECT id_equipe, nom_equipe FROM t_equipe WHERE id_equipe = %(value_id_equipe)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode"mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_equipe, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_select_equipe_data Gestions equipe numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_equipe_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_equipe_data d\'un equipe Data Gestions equipe {erreur}")

    def delete_equipe_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "equipeEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnomequipeHTML" value="{{ row.nom_equipe }}"/></td>
            str_sql_delete_nomequipe = "DELETE FROM t_equipe WHERE id_equipe = %(value_id_equipe)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_nomequipe, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_equipe_data Data Gestions equipe numéro de l'erreur : {erreur}")
            flash(f"Flash. Problèmes Data Gestions equipe numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # OM 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un equipe qui est associé à un personne dans la table intermédiaire "t_personne_equipe"
                # il y a une contrainte sur les FK de la table intermédiaire "t_personne_equipe"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce equipe est associé à des personne dans la t_personne_equipe !!! : {erreur}", "danger")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !!! Ce equipe est associé à des personne dans la t_personne_equipe !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")
