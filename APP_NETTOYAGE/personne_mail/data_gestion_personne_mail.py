# data_gestion_personne_mail.py
# OM 2020.04.22 Permet de gérer (CRUD) les données de la table intermédiaire "t_personne_avoir_mail"

from flask import flash
from APP_NETTOYAGE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_NETTOYAGE.DATABASE.erreurs import *


class Gestionpersonnemail():
    def __init__ (self):
        try:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print("dans le try de gestions mail")
            # OM 2020.04.11 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion mail personne ...terrible erreur, il faut connecter une base de donnée", "danger")
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur Gestionpersonnemail {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur Gestionpersonnemail ")

    def mail_afficher_data (self):
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_mail"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher
            strsql_mail_afficher = """SELECT id_mail, adresse_mail FROM t_mail ORDER BY id_mail ASC"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_mail_afficher)
                # Récupère les données de la requête.
                data_mail = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_mail ", data_mail, " Type : ", type(data_mail))
                # Retourne les données du "SELECT"
                return data_mail
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def personne_mail_afficher_data (self, valeur_id_personne_selected_dict):
        print("valeur_id_personne_selected_dict8..", valeur_id_personne_selected_dict)
        try:

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_mail"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_personne_selected = """SELECT id_personne, nom_personne, prenom_personne, GROUP_CONCAT(id_mail) as mailpersonne FROM t_personne_avoir_mail AS T1
                                        INNER JOIN t_personne AS T2 ON T2.id_personne = T1.FK_personne
                                        INNER JOIN t_mail AS T3 ON T3.id_mail = T1.FK_mail
                                        WHERE id_personne = %(value_id_personne_selected)s"""

            strsql_personne_mail_non_attribues = """SELECT id_mail, adresse_mail FROM t_mail
                                                    WHERE id_mail not in(SELECT id_mail as idmailpersonne FROM t_personne_avoir_mail AS T1
                                                    INNER JOIN t_personne AS T2 ON T2.id_personne = T1.FK_personne
                                                    INNER JOIN t_mail AS T3 ON T3.id_mail = T1.FK_mail
                                                    WHERE id_personne = %(value_id_personne_selected)s)"""

            strsql_personne_mail_attribues = """SELECT id_personne, id_mail, adresse_mail FROM t_personne_avoir_mail AS T1
                                            INNER JOIN t_personne AS T2 ON T2.id_personne = T1.FK_personne
                                            INNER JOIN t_mail AS T3 ON T3.id_mail = T1.FK_mail
                                            WHERE id_personne = %(value_id_personne_selected)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_personne_mail_non_attribues, valeur_id_personne_selected_dict)
                # Récupère les données de la requête.
                data_personne_mail_non_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("dfad data_personne_mail_non_attribues ", data_personne_mail_non_attribues, " Type : ",
                      type(data_personne_mail_non_attribues))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_personne_selected, valeur_id_personne_selected_dict)
                # Récupère les données de la requête.
                data_personne_selected = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_personne_selected  ", data_personne_selected, " Type : ", type(data_personne_selected))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_personne_mail_attribues, valeur_id_personne_selected_dict)
                # Récupère les données de la requête.
                data_personne_mail_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_personne_mail_attribues ", data_personne_mail_attribues, " Type : ",
                      type(data_personne_mail_attribues))

                # Retourne les données du "SELECT"
                return data_personne_selected, data_personne_mail_non_attribues, data_personne_mail_attribues
        except pymysql.Error as erreur:
            print(f"DGGF gfad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def personne_mail_afficher_data_concat (self, id_personne_selected):
        print("id_personne_selected  ", id_personne_selected)
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_mail"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_personne_mail_afficher_data_concat = """SELECT id_personne, nom_personne, prenom_personne,
                                                            GROUP_CONCAT(adresse_mail) as mailpersonne FROM t_personne_avoir_mail AS T1
                                                            RIGHT JOIN t_personne AS T2 ON T2.id_personne = T1.FK_personne
                                                            LEFT JOIN t_mail AS T3 ON T3.id_mail = T1.FK_mail
                                                            GROUP BY id_personne"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # le paramètre 0 permet d'afficher tous les personne
                # Sinon le paramètre représente la valeur de l'id du personne
                if id_personne_selected == 0:
                    mc_afficher.execute(strsql_personne_mail_afficher_data_concat)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                    valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_personne_selected}
                    strsql_personne_mail_afficher_data_concat += """ HAVING id_personne= %(value_id_personne_selected)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_personne_mail_afficher_data_concat, valeur_id_personne_selected_dictionnaire)

                # Récupère les données de la requête.
                data_personne_mail_afficher_concat = mc_afficher.fetchall()
                # Affichage dans la console
                print("dggf data_personne_mail_afficher_concat ", data_personne_mail_afficher_concat, " Type : ",
                      type(data_personne_mail_afficher_concat))

                # Retourne les données du "SELECT"
                return data_personne_mail_afficher_concat


        except pymysql.Error as erreur:
            print(f"DGGF gfadc pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfadc Exception {erreur.args}")
            raise MaBdErreurConnexion(
                f"DGG gfadc Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfadc pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def personne_mail_add (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_personne" et "id_mail" dans la "t_personne_avoir_mail"
            strsql_insert_personne_avoir_mail = """INSERT INTO t_personne_avoir_mail (id_personne_avoir_mail, FK_mail, FK_personne)
                                            VALUES (NULL, %(value_FK_mail)s, %(value_FK_personne)s)"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_personne_avoir_mail, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

    def personne_mail_delete (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Effacer une (des) association(s) existantes entre "id_personne" et "id_mail" dans la "t_personne_avoir_mail"
            strsql_delete_personne_mail = """DELETE FROM t_personne_avoir_mail WHERE FK_mail = %(value_FK_mail)s AND FK_personne = %(value_FK_personne)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_delete_personne_mail, valeurs_insertion_dictionnaire)
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème personne_mail_delete Gestions mail personne numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème personne_mail_delete Gestions mail personne  numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème personne_mail_delete Gestions mail personne  {erreur}")

    def edit_mail_data (self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le mail sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_personnes = "SELECT id_mail, adresse_mail FROM t_mail WHERE id_mail = %(value_id_mail)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_personnes, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_mail_data Data Gestions mail numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions mail numéro de l'erreur : {erreur}", "danger")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_mail_data d'un mail Data Gestions mail {erreur}")

    def update_mail_data (self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditnommailHTML" du form HTML "mailEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnommailHTML" value="{{ row.nom_mail }}"/></td>
            str_sql_update_nommail = "UPDATE t_mail SET adresse_mail = %(value_name_genre)s WHERE id_mail = %(value_id_mail)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_nommail, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_mail_data Data Gestions mail numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions mail numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_mail_data d\'un mail Data Gestions mail {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "warning")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash(f"Doublon !!! Introduire une valeur différente", "warning")
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_mail_data Data Gestions mail numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_mail_data d'un mail DataGestionsmail {erreur}")

    def delete_select_mail_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditnommailHTML" du form HTML "mailEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnommailHTML" value="{{ row.nom_mail }}"/></td>

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le mail sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_mail = "SELECT id_mail, adresse_mail FROM t_mail WHERE id_mail = %(value_id_mail)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode"mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_mail, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_select_mail_data Gestions mail numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_mail_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_mail_data d\'un mail Data Gestions mail {erreur}")

    def delete_mail_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "mailEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnommailHTML" value="{{ row.nom_mail }}"/></td>
            str_sql_delete_nommail = "DELETE FROM t_mail WHERE id_mail = %(value_id_mail)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_nommail, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_mail_data Data Gestions mail numéro de l'erreur : {erreur}")
            flash(f"Flash. Problèmes Data Gestions mail numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # OM 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un mail qui est associé à un personne dans la table intermédiaire "t_personne_avoir_mail"
                # il y a une contrainte sur les FK de la table intermédiaire "t_personne_avoir_mail"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce mail est associé à des personne dans la t_personne_avoir_mail !!! : {erreur}", "danger")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !!! Ce mail est associé à des personne dans la t_personne_avoir_mail !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")
