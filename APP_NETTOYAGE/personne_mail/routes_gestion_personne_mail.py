# routes_gestion_personne_mail.py
# OM 2020.04.16 Gestions des "routes" FLASK pour la table intermédiaire qui associe les personne et les mail.

from flask import render_template, request, flash, session
from APP_NETTOYAGE import obj_mon_application
from APP_NETTOYAGE.mail.data_gestion_mail import Gestionmail
from APP_NETTOYAGE.personne_mail.data_gestion_personne_mail import Gestionpersonnemail


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /personne_mail_afficher_concat
# Récupère la liste de tous les personne et de tous les mail associés aux personne.
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/personne_mail_afficher_concat/<int:id_personne_sel>", methods=['GET', 'POST'])
def personne_mail_afficher_concat (id_personne_sel):
    print("id_personne_sel ", id_personne_sel)
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = Gestionpersonnemail()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionmail()
            # Fichier data_gestion_mail.py
            data_personne_mail_afficher_concat = obj_actions_mail.personne_mail_afficher_data_concat(id_personne_sel)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data mail", data_personne_mail_afficher_concat, "type ", type(data_personne_mail_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_personne_mail_afficher_concat:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données mail affichés dans mailpersonne!!", "success")
            else:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_personne_mail" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("personne_mail/personne_mail_afficher.html",
                           data=data_personne_mail_afficher_concat)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.21 Définition d'une "route" /gf_edit_personne_mail_selected
# Récupère la liste de tous les mail du personne sélectionné.
# Nécessaire pour afficher tous les "TAGS" des mail, ainsi l'utilisateur voit les mail à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_edit_personne_mail_selected", methods=['GET', 'POST'])
def gf_edit_personne_mail_selected ():
    if request.method == "GET":
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = Gestionmail()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionmail()
            # Fichier data_gestion_mail.py
            # Pour savoir si la table "t_mail" est vide, ainsi on empêche l’affichage des tags
            # dans le render_template(personne_mail_modifier_tags_dropbox.html)
            data_mail_all = obj_actions_mail.mail_afficher_data()

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données de la table intermédiaire.
            obj_actions_mail = Gestionpersonnemail()

            # OM 2020.04.21 Récupère la valeur de "id_personne" du formulaire html "personne_mail_afficher.html"
            # l'utilisateur clique sur le lien "Modifier mail de ce personne" et on récupère la valeur de "id_personne" grâce à la variable "id_personne_avoir_mail_edit_html"
            # <a href="{{ url_for('gf_edit_personne_mail_selected', id_personne_avoir_mail_edit_html=row.id_personne) }}">Modifier les mail de ce personne</a>
            id_personne_avoir_mail_edit = request.values['id_personne_avoir_mail_edit_html']

            # OM 2020.04.21 Mémorise l'id du personne dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_personne_avoir_mail_edit'] = id_personne_avoir_mail_edit

            # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
            valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_personne_avoir_mail_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la classe Gestionpersonnemail()
            # 1) Sélection du personne choisi
            # 2) Sélection des mail "déjà" attribués pour le personne.
            # 3) Sélection des mail "pas encore" attribués pour le personne choisi.
            # Fichier data_gestion_personne_mail.py
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "personne_mail_afficher_data"
            data_personne_mail_selected, data_personne_mail_non_attribues, data_personne_mail_attribues = \
                obj_actions_mail.personne_mail_afficher_data(valeur_id_personne_selected_dictionnaire)

            lst_data_personne_selected = [item['id_personne'] for item in data_personne_mail_selected]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_selected  ", lst_data_personne_selected,
                  type(lst_data_personne_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les mail qui ne sont pas encore sélectionnés.
            lst_data_personne_mail_non_attribues = [item['id_mail'] for item in data_personne_mail_non_attribues]
            session['session_lst_data_personne_mail_non_attribues'] = lst_data_personne_mail_non_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_mail_non_attribues  ", lst_data_personne_mail_non_attribues,
                  type(lst_data_personne_mail_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les mail qui sont déjà sélectionnés.
            lst_data_personne_mail_old_attribues = [item['id_mail'] for item in data_personne_mail_attribues]
            session['session_lst_data_personne_mail_old_attribues'] = lst_data_personne_mail_old_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_mail_old_attribues  ", lst_data_personne_mail_old_attribues,
                  type(lst_data_personne_mail_old_attribues))

            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data data_personne_mail_selected", data_personne_mail_selected, "type ", type(data_personne_mail_selected))
            print(" data data_personne_mail_non_attribues ", data_personne_mail_non_attribues, "type ",
                  type(data_personne_mail_non_attribues))
            print(" data_personne_mail_attribues ", data_personne_mail_attribues, "type ",
                  type(data_personne_mail_attribues))

            # Extrait les valeurs contenues dans la table "t_mail", colonne "nom_mail"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_mail
            lst_data_personne_mail_non_attribues = [item['adresse_mail'] for item in data_personne_mail_non_attribues]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_all_mail gf_edit_personne_mail_selected ", lst_data_personne_mail_non_attribues,
                  type(lst_data_personne_mail_non_attribues))

            # Différencier les messages si la table est vide.
            if lst_data_personne_selected == [None]:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_personne_mail" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données mail affichées dans mailpersonne!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("personne_mail/personne_mail_modifier_tags_dropbox.html",
                           data_mail=data_mail_all,
                           data_personne_selected=data_personne_mail_selected,
                           data_mail_attribues=data_personne_mail_attribues,
                           data_mail_non_attribues=data_personne_mail_non_attribues)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /gf_update_personne_mail_selected
# Récupère la liste de tous les mail du personne sélectionné.
# Nécessaire pour afficher tous les "TAGS" des mail, ainsi l'utilisateur voit les mail à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_update_personne_mail_selected", methods=['GET', 'POST'])
def gf_update_personne_mail_selected ():
    if request.method == "POST":
        try:
            # Récupère l'id du personne sélectionné
            id_personne_selected = session['session_id_personne_avoir_mail_edit']
            print("session['session_id_personne_avoir_mail_edit'] ", session['session_id_personne_avoir_mail_edit'])

            # Récupère la liste des mail qui ne sont pas associés au personne sélectionné.
            old_lst_data_personne_mail_non_attribues = session['session_lst_data_personne_mail_non_attribues']
            print("old_lst_data_personne_mail_non_attribues ", old_lst_data_personne_mail_non_attribues)

            # Récupère la liste des mail qui sont associés au personne sélectionné.
            old_lst_data_personne_mail_attribues = session['session_lst_data_personne_mail_old_attribues']
            print("old_lst_data_personne_mail_old_attribues ", old_lst_data_personne_mail_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme mail dans le composant "tags-selector-tagselect"
            # dans le fichier "personne_mail_modifier_tags_dropbox.html"
            new_lst_str_personne_mail = request.form.getlist('name_select_tags')
            print("new_lst_str_personne_mail ", new_lst_str_personne_mail)

            # OM 2020.04.29 Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_personne_mail_old = list(map(int, new_lst_str_personne_mail))
            print("new_lst_personne_mail ", new_lst_int_personne_mail_old, "type new_lst_personne_mail ",
                  type(new_lst_int_personne_mail_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2020.04.29 Une liste de "id_mail" qui doivent être effacés de la table intermédiaire "t_personne_mail".
            lst_diff_mail_delete_b = list(
                set(old_lst_data_personne_mail_attribues) - set(new_lst_int_personne_mail_old))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_mail_delete_b ", lst_diff_mail_delete_b)

            # OM 2020.04.29 Une liste de "id_mail" qui doivent être ajoutés à la BD
            lst_diff_mail_insert_a = list(
                set(new_lst_int_personne_mail_old) - set(old_lst_data_personne_mail_attribues))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_mail_insert_a ", lst_diff_mail_insert_a)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = Gestionpersonnemail()

            # Pour le personne sélectionné, parcourir la liste des mail à INSÉRER dans la "t_personne_mail".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_mail_ins in lst_diff_mail_insert_a:
                # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                # et "id_mail_ins" (l'id du mail dans la liste) associé à une variable.
                valeurs_personne_sel_mail_sel_dictionnaire = {"value_FK_personne": id_personne_selected,
                                                           "value_FK_mail": id_mail_ins}
                # Insérer une association entre un(des) mail(s) et le personne sélectionner.
                obj_actions_mail.personne_mail_add(valeurs_personne_sel_mail_sel_dictionnaire)

            # Pour le personne sélectionné, parcourir la liste des mail à EFFACER dans la "t_personne_mail".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_mail_del in lst_diff_mail_delete_b:
                # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                # et "id_mail_del" (l'id du mail dans la liste) associé à une variable.
                valeurs_personne_sel_mail_sel_dictionnaire = {"value_FK_personne": id_personne_selected,
                                                           "value_FK_mail": id_mail_del}
                # Effacer une association entre un(des) mail(s) et le personne sélectionner.
                obj_actions_mail.personne_mail_delete(valeurs_personne_sel_mail_sel_dictionnaire)

            # Récupère les données grâce à une requête MySql définie dans la classe Gestionmail()
            # Fichier data_gestion_mail.py
            # Afficher seulement le personne dont les mail sont modifiés, ainsi l'utilisateur voit directement
            # les changements qu'il a demandés.
            data_personne_mail_afficher_concat = obj_actions_mail.personne_mail_afficher_data_concat(id_personne_selected)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data mail", data_personne_mail_afficher_concat, "type ", type(data_personne_mail_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_personne_mail_afficher_concat == None:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_personne_mail" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données mail affichées dans mailpersonne!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Après cette mise à jour de la table intermédiaire "t_personne_mail",
    # on affiche les personne et le(urs) mail(s) associé(s).
    return render_template("personne_mail/personne_mail_afficher.html",
                           data=data_personne_mail_afficher_concat)
