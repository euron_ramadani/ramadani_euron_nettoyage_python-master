# routes_gestion_materiel.py
# OM 2020.04.06 Gestions des "routes" FLASK pour les materiel.

import pymysql
from flask import render_template, flash, redirect, url_for, request
from APP_NETTOYAGE import obj_mon_application
from APP_NETTOYAGE.materiel.data_gestion_materiel import Gestionmateriel
from APP_NETTOYAGE.DATABASE.erreurs import *
import re

@obj_mon_application.route("/materiel_afficher")
def materiel_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_materiel = Gestionmateriel()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionmateriel()
            # Fichier data_gestion_materiel.py
            data_materiel = obj_actions_materiel.materiel_afficher_data()
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data materiel", data_materiel, "type ", type(data_materiel))
            # Différencier les messages si la table est vide.
            if data_materiel:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données Objet affichées !!", "success")
            else:
                flash("""La table "t_materiel" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}","danger")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("materiel/materiel_afficher.html", data=data_materiel)


# OM 2020.04.06 Pour une simple démo. On insère deux fois des valeurs dans la table materiel
# Une fois de manière fixe, vous devez changer les valeurs pour voir le résultat dans la table "t_materiel"
# La 2ème il faut entrer la valeur du titre du materiel par le clavier, il ne doit pas être vide.
# Pour les autres valeurs elles doivent être changées ci-dessous.
# Une des valeurs est "None" ce qui en MySql donne "NULL" pour l'attribut "t_materiel.cover_link_materiel"
# Pour la tester http://127.0.0.1:5005/materiel_add
@obj_mon_application.route("/materiel_add", methods=['GET', 'POST'])
def materiel_add ():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_materiel = Gestionmateriel()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "materiel_add.html"
            produit_materiel = request.form['produit_materiel_html']
            marque_materiel = request.form['marque_materiel_html']
            print("Ici ok")
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            # if not re.match("^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$",
            #                 materiel):
            #     print("Ici OK")
            #     # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
            #     flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
            #           f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")
            #     # On doit afficher à nouveau le formulaire "materiel_add.html" à cause des erreurs de "claviotage"
            #     return render_template("materiel/materiel_add.html")
            # else:

            # Constitution d'un dictionnaire et insertion dans la BD
            valeurs_insertion_dictionnaire = {"value_produit_materiel": produit_materiel,
                                              "value_marque_materiel": marque_materiel}
            obj_actions_materiel.add_materiel_data(valeurs_insertion_dictionnaire)

            # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Données insérées !!", "success")
            print(f"Données insérées !!")
            # On va interpréter la "route" 'materiel_afficher', car l'utilisateur
            # doit voir le nouveau materiel qu'il vient d'insérer. Et on l'affiche de manière
            # à voir le dernier élément inséré.
            return redirect(url_for('materiel_afficher', order_by = 'DESC', id_materiel_sel=0))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("materiel/materiel_add.html")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /materiel_edit ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un materiel de materiel par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/materiel_edit', methods=['POST', 'GET'])
def materiel_edit ():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "materiel_afficher.html"
    if request.method == 'GET':
        try:
            # Récupère la valeur de "id_materiel" du formulaire html "materiel_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_materiel"
            # grâce à la variable "id_materiel_edit_html"
            # <a href="{{ url_for('materiel_edit', id_materiel_edit_html=row.id_materiel) }}">Edit</a>
            id_materiel_edit = request.values['id_materiel_edit_html']

            # Pour afficher dans la console la valeur de "id_materiel_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(id_materiel_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_materiel": id_materiel_edit}

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_materiel = Gestionmateriel()

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_materiel = obj_actions_materiel.edit_materiel_data(valeur_select_dictionnaire)
            print("dataidmateriel ", data_id_materiel, "type ", type(data_id_materiel))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le materiel d'un materiel !!!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("materiel/materiel_edit.html", data=data_id_materiel)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /materiel_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un materiel de materiel par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/materiel_update', methods=['POST', 'GET'])
def materiel_update ():
    # DEBUG bon marché : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "materiel_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du materiel alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # DEBUG bon marché : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ", request.values)

            # Récupère la valeur de "id_materiel" du formulaire html "materiel_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_materiel"
            # grâce à la variable "id_materiel_edit_html"
            # <a href="{{ url_for('materiel_edit', id_materiel_edit_html=row.id_materiel) }}">Edit</a>
            id_materiel_edit = request.values['id_materiel_edit_html']

            # Récupère le contenu du champ "materiel" dans le formulaire HTML "materielEdit.html"
            produit_materiel = request.values['edit_produit_materiel_html']
            marque_materiel = request.values['edit_marque_materiel_html']
            valeur_edit_list = [{'id_materiel': id_materiel_edit, 'produit_materiel': produit_materiel
                                 ,'marque_materiel': marque_materiel}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$",
                            produit_materiel, marque_materiel):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "materiel" dans le formulaire HTML "materielEdit.html"
                # materiel = request.values['name_edit_materiel_html']
                # Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")

                # On doit afficher à nouveau le formulaire "materiel_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "materiel_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_materiel': 13, 'materiel': 'philosophique'}]
                valeur_edit_list = [{'id_materiel': id_materiel_edit, 'produit_materiel': produit_materiel
                                     ,'marque_materiel': marque_materiel}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "materiel_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('materiel/materiel_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_materiel": id_materiel_edit, "value_produit_materiel": produit_materiel,
                                              "value_marque_materiel": marque_materiel}

                # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_materiel = Gestionmateriel()

                # La commande MySql est envoyée à la BD
                data_id_materiel = obj_actions_materiel.update_materiel_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataidmateriel ", data_id_materiel, "type ", type(data_id_materiel))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Valeur materiel modifiée. ", "success")
                # On affiche les materiel avec celui qui vient d'être edité en tête de liste. (DESC)
                return redirect(url_for('materiel_afficher', order_by="ASC", id_materiel_sel=id_materiel_edit))

        except (Exception,
                # pymysql.err.OperationalError,
                # pymysql.ProgrammingError,
                # pymysql.InternalError,
                # pymysql.IntegrityError,
                TypeError) as erreur:
            print(erreur.args[0])
            flash(f"problème materiel ____lllupdate{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_materiel_html" alors on renvoie le formulaire "EDIT"
    return render_template('materiel/materiel_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /materiel_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un materiel de materiel par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/materiel_select_delete', methods=['POST', 'GET'])
def materiel_select_delete ():
    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_materiel = Gestionmateriel()
            # OM 2019.04.04 Récupère la valeur de "idmaterielDeleteHTML" du formulaire html "materielDelete.html"
            id_materiel_delete = request.args.get('id_materiel_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_materiel": id_materiel_delete}

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_materiel = obj_actions_materiel.delete_select_materiel_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur materiel_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur materiel_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('materiel/materiel_delete.html', data=data_id_materiel)


# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /materielUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un materiel, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/materiel_delete', methods=['POST', 'GET'])
def materiel_delete ():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_materiel = Gestionmateriel()
            # OM 2019.04.02 Récupère la valeur de "id_materiel" du formulaire html "materielAfficher.html"
            id_materiel_delete = request.form['id_materiel_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_materiel": id_materiel_delete}

            data_materiel = obj_actions_materiel.delete_materiel_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des materiel des materiel
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les materiel
            return redirect(url_for('materiel_afficher',order_by="ASC",id_materiel_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "materiel" de materiel qui est associé dans "t_materiel_materiel".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des materiel !', "warning")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce materiel est associé à des materiel dans la t_materiel_materiel !!! : {erreur}")
                # Afficher la liste des materiel des materiel
                return redirect(url_for('materiel_afficher', order_by="ASC", id_materiel_sel=0))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur materiel_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur materiel_delete {erreur.args[0], erreur.args[1]}", "danger")

            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('materiel/materiel_afficher.html', data=data_materiel)

