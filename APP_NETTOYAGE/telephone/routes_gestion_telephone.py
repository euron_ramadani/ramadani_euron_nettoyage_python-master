# routes_gestion_telephone.py
# OM 2020.04.06 Gestions des "routes" FLASK pour les telephone.

import pymysql
from flask import render_template, flash, redirect, url_for, request
from APP_NETTOYAGE import obj_mon_application
from APP_NETTOYAGE.telephone.data_gestion_telephone import Gestiontelephone
from APP_NETTOYAGE.DATABASE.erreurs import *
import re

@obj_mon_application.route("/telephone_afficher")
def telephone_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_telephone = Gestiontelephone()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestiontelephone()
            # Fichier data_gestion_telephone.py
            data_telephone = obj_actions_telephone.telephone_afficher_data()
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data telephone", data_telephone, "type ", type(data_telephone))
            # Différencier les messages si la table est vide.
            if data_telephone:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données Objet affichées !!", "success")
            else:
                flash("""La table "t_telephone" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}","danger")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("telephone/telephone_afficher.html", data=data_telephone)


# OM 2020.04.06 Pour une simple démo. On insère deux fois des valeurs dans la table telephone
# Une fois de manière fixe, vous devez changer les valeurs pour voir le résultat dans la table "t_telephone"
# La 2ème il faut entrer la valeur du titre du telephone par le clavier, il ne doit pas être vide.
# Pour les autres valeurs elles doivent être changées ci-dessous.
# Une des valeurs est "None" ce qui en MySql donne "NULL" pour l'attribut "t_telephone.cover_link_telephone"
# Pour la tester http://127.0.0.1:5005/telephone_add
@obj_mon_application.route("/telephone_add", methods=['GET', 'POST'])
def telephone_add ():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_telephone = Gestiontelephone()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "telephone_add.html"
            numero_telephone = request.form['numero_telephone_html']
            print("Ici ok")
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            # if not re.match("^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$",
            #                 telephone):
            #     print("Ici OK")
            #     # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
            #     flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
            #           f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")
            #     # On doit afficher à nouveau le formulaire "telephone_add.html" à cause des erreurs de "claviotage"
            #     return render_template("telephone/telephone_add.html")
            # else:

            # Constitution d'un dictionnaire et insertion dans la BD
            valeurs_insertion_dictionnaire = {"value_numero_telephone": numero_telephone}
            obj_actions_telephone.add_telephone_data(valeurs_insertion_dictionnaire)

            # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Données insérées !!", "success")
            print(f"Données insérées !!")
            # On va interpréter la "route" 'telephone_afficher', car l'utilisateur
            # doit voir le nouveau telephone qu'il vient d'insérer. Et on l'affiche de manière
            # à voir le dernier élément inséré.
            return redirect(url_for('telephone_afficher', order_by = 'DESC', id_telephone_sel=0))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("telephone/telephone_add.html")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /telephone_edit ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un telephone de adresse par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/telephone_edit', methods=['POST', 'GET'])
def telephone_edit ():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "telephone_afficher.html"
    if request.method == 'GET':
        try:
            # Récupère la valeur de "id_telephone" du formulaire html "telephone_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_telephone"
            # grâce à la variable "id_telephone_edit_html"
            # <a href="{{ url_for('telephone_edit', id_telephone_edit_html=row.id_telephone) }}">Edit</a>
            id_telephone_edit = request.values['id_telephone_edit_html']

            # Pour afficher dans la console la valeur de "id_telephone_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(id_telephone_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_telephone": id_telephone_edit}

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_telephone = Gestiontelephone()

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_telephone = obj_actions_telephone.edit_telephone_data(valeur_select_dictionnaire)
            print("dataidtelephone ", data_id_telephone, "type ", type(data_id_telephone))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le telephone d'un adresse !!!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("telephone/telephone_edit.html", data=data_id_telephone)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /telephone_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un telephone de adresse par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/telephone_update', methods=['POST', 'GET'])
def telephone_update ():
    # DEBUG bon marché : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "telephone_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du telephone alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # DEBUG bon marché : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ", request.values)

            # Récupère la valeur de "id_telephone" du formulaire html "telephone_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_telephone"
            # grâce à la variable "id_telephone_edit_html"
            # <a href="{{ url_for('telephone_edit', id_telephone_edit_html=row.id_telephone) }}">Edit</a>
            id_telephone_edit = request.values['id_telephone_edit_html']

            # Récupère le contenu du champ "numero_telephone" dans le formulaire HTML "telephoneEdit.html"
            numero_telephone = request.values['edit_numero_telephone_html']
            valeur_edit_list = [{'id_telephone': id_telephone_edit, 'numero_telephone': numero_telephone}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^(\+41|0041|0){1}(\(0\))?[0-9]{9}$",
                            numero_telephone):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "numero_telephone" dans le formulaire HTML "telephoneEdit.html"
                # numero_telephone = request.values['name_edit_numero_telephone_html']
                # Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de lettres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")

                # On doit afficher à nouveau le formulaire "telephone_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "telephone_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_telephone': 13, 'numero_telephone': 'philosophique'}]
                valeur_edit_list = [{'id_telephone': id_telephone_edit, 'numero_telephone': numero_telephone}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "telephone_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('telephone/telephone_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_telephone": id_telephone_edit, "value_numero_telephone": numero_telephone}

                # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_telephone = Gestiontelephone()

                # La commande MySql est envoyée à la BD
                data_id_telephone = obj_actions_telephone.update_telephone_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataidtelephone ", data_id_telephone, "type ", type(data_id_telephone))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Valeur telephone modifiée. ", "success")
                # On affiche les telephone avec celui qui vient d'être edité en tête de liste. (DESC)
                return redirect(url_for('telephone_afficher', order_by="ASC", id_telephone_sel=id_telephone_edit))

        except (Exception,
                # pymysql.err.OperationalError,
                # pymysql.ProgrammingError,
                # pymysql.InternalError,
                # pymysql.IntegrityError,
                TypeError) as erreur:
            print(erreur.args[0])
            flash(f"problème telephone ____lllupdate{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_numero_telephone_html" alors on renvoie le formulaire "EDIT"
    return render_template('telephone/telephone_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /telephone_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un telephone de telephone par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/telephone_select_delete', methods=['POST', 'GET'])
def telephone_select_delete ():
    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_telephone = Gestiontelephone()
            # OM 2019.04.04 Récupère la valeur de "idtelephoneDeleteHTML" du formulaire html "telephoneDelete.html"
            id_telephone_delete = request.args.get('id_telephone_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_telephone": id_telephone_delete}

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_telephone = obj_actions_telephone.delete_select_telephone_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur telephone_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur telephone_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('telephone/telephone_delete.html', data=data_id_telephone)


# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /telephoneUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un telephone, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/telephone_delete', methods=['POST', 'GET'])
def telephone_delete ():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_telephone = Gestiontelephone()
            # OM 2019.04.02 Récupère la valeur de "id_telephone" du formulaire html "telephoneAfficher.html"
            id_telephone_delete = request.form['id_telephone_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_telephone": id_telephone_delete}

            data_telephone = obj_actions_telephone.delete_telephone_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des telephone des telephone
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les telephone
            return redirect(url_for('telephone_afficher',order_by="ASC",id_telephone_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "telephone" de telephone qui est associé dans "t_telephone_telephone".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des telephone !', "warning")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce telephone est associé à des telephone dans la t_telephone_telephone !!! : {erreur}")
                # Afficher la liste des telephone des telephone
                return redirect(url_for('telephone_afficher', order_by="ASC", id_telephone_sel=0))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur telephone_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur telephone_delete {erreur.args[0], erreur.args[1]}", "danger")

            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('telephone/telephone_afficher.html', data=data_telephone)

