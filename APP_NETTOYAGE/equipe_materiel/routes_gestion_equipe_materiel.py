# routes_gestion_equipe_materiel.py
# OM 2020.04.16 Gestions des "routes" FLASK pour la table intermédiaire qui associe les equipe et les materiel.

from flask import render_template, request, flash, session
from APP_NETTOYAGE import obj_mon_application
from APP_NETTOYAGE.materiel.data_gestion_materiel import Gestionmateriel
from APP_NETTOYAGE.equipe_materiel.data_gestion_equipe_materiel import Gestionequipemateriel


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /equipe_materiel_afficher_concat
# Récupère la liste de tous les equipe et de tous les materiel associés aux equipe.
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/equipe_materiel_afficher_concat/<int:id_equipe_sel>", methods=['GET', 'POST'])
def equipe_materiel_afficher_concat (id_equipe_sel):
    print("id_equipe_sel ", id_equipe_sel)
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_materiel = Gestionequipemateriel()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionmateriel()
            # Fichier data_gestion_materiel.py
            data_equipe_materiel_afficher_concat = obj_actions_materiel.equipe_materiel_afficher_data_concat(id_equipe_sel)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data materiel", data_equipe_materiel_afficher_concat, "type ", type(data_equipe_materiel_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_equipe_materiel_afficher_concat:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données materiel affichés dans materielequipe!!", "success")
            else:
                flash(f"""Le equipe demandé n'existe pas. Ou la table "t_equipe_materiel" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("equipe_materiel/equipe_materiel_afficher.html",
                           data=data_equipe_materiel_afficher_concat)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.21 Définition d'une "route" /gf_edit_equipe_materiel_selected
# Récupère la liste de tous les materiel du equipe sélectionné.
# Nécessaire pour afficher tous les "TAGS" des materiel, ainsi l'utilisateur voit les materiel à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_edit_equipe_materiel_selected", methods=['GET', 'POST'])
def gf_edit_equipe_materiel_selected ():
    if request.method == "GET":
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_materiel = Gestionmateriel()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionmateriel()
            # Fichier data_gestion_materiel.py
            # Pour savoir si la table "t_materiel" est vide, ainsi on empêche l’affichage des tags
            # dans le render_template(equipe_materiel_modifier_tags_dropbox.html)
            data_materiel_all = obj_actions_materiel.materiel_afficher_data()

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données de la table intermédiaire.
            obj_actions_materiel = Gestionequipemateriel()

            # OM 2020.04.21 Récupère la valeur de "id_equipe" du formulaire html "equipe_materiel_afficher.html"
            # l'utilisateur clique sur le lien "Modifier materiel de ce equipe" et on récupère la valeur de "id_equipe" grâce à la variable "id_equipe_materiel_edit_html"
            # <a href="{{ url_for('gf_edit_equipe_materiel_selected', id_equipe_materiel_edit_html=row.id_equipe) }}">Modifier les materiel de ce equipe</a>
            id_equipe_materiel_edit = request.values['id_equipe_materiel_edit_html']

            # OM 2020.04.21 Mémorise l'id du equipe dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_equipe_materiel_edit'] = id_equipe_materiel_edit

            # Constitution d'un dictionnaire pour associer l'id du equipe sélectionné avec un nom de variable
            valeur_id_equipe_selected_dictionnaire = {"value_id_equipe_selected": id_equipe_materiel_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la classe Gestionequipemateriel()
            # 1) Sélection du equipe choisi
            # 2) Sélection des materiel "déjà" attribués pour le equipe.
            # 3) Sélection des materiel "pas encore" attribués pour le equipe choisi.
            # Fichier data_gestion_equipe_materiel.py
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "equipe_materiel_afficher_data"
            data_equipe_materiel_selected, data_equipe_materiel_non_attribues, data_equipe_materiel_attribues = \
                obj_actions_materiel.equipe_materiel_afficher_data(valeur_id_equipe_selected_dictionnaire)

            lst_data_equipe_selected = [item['id_equipe'] for item in data_equipe_materiel_selected]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_equipe_selected  ", lst_data_equipe_selected,
                  type(lst_data_equipe_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les materiel qui ne sont pas encore sélectionnés.
            lst_data_equipe_materiel_non_attribues = [item['id_materiel'] for item in data_equipe_materiel_non_attribues]
            session['session_lst_data_equipe_materiel_non_attribues'] = lst_data_equipe_materiel_non_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_equipe_materiel_non_attribues  ", lst_data_equipe_materiel_non_attribues,
                  type(lst_data_equipe_materiel_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les materiel qui sont déjà sélectionnés.
            lst_data_equipe_materiel_old_attribues = [item['id_materiel'] for item in data_equipe_materiel_attribues]
            session['session_lst_data_equipe_materiel_old_attribues'] = lst_data_equipe_materiel_old_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_equipe_materiel_old_attribues  ", lst_data_equipe_materiel_old_attribues,
                  type(lst_data_equipe_materiel_old_attribues))

            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data data_equipe_materiel_selected", data_equipe_materiel_selected, "type ", type(data_equipe_materiel_selected))
            print(" data data_equipe_materiel_non_attribues ", data_equipe_materiel_non_attribues, "type ",
                  type(data_equipe_materiel_non_attribues))
            print(" data_equipe_materiel_attribues ", data_equipe_materiel_attribues, "type ",
                  type(data_equipe_materiel_attribues))

            # Extrait les valeurs contenues dans la table "t_materiel", colonne "nom_materiel"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_materiel
            lst_data_equipe_materiel_non_attribues = [item['produit_materiel'] for item in data_equipe_materiel_non_attribues]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_all_materiel gf_edit_equipe_materiel_selected ", lst_data_equipe_materiel_non_attribues,
                  type(lst_data_equipe_materiel_non_attribues))

            # Différencier les messages si la table est vide.
            if lst_data_equipe_selected == [None]:
                flash(f"""Le equipe demandé n'existe pas. Ou la table "t_equipe_materiel" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données materiel affichées dans materielequipe!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("equipe_materiel/equipe_materiel_modifier_tags_dropbox.html",
                           data_materiel=data_materiel_all,
                           data_equipe_selected=data_equipe_materiel_selected,
                           data_materiel_attribues=data_equipe_materiel_attribues,
                           data_materiel_non_attribues=data_equipe_materiel_non_attribues)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /gf_update_equipe_materiel_selected
# Récupère la liste de tous les materiel du equipe sélectionné.
# Nécessaire pour afficher tous les "TAGS" des materiel, ainsi l'utilisateur voit les materiel à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_update_equipe_materiel_selected", methods=['GET', 'POST'])
def gf_update_equipe_materiel_selected ():
    if request.method == "POST":
        try:
            # Récupère l'id du equipe sélectionné
            id_equipe_selected = session['session_id_equipe_materiel_edit']
            print("session['session_id_equipe_materiel_edit'] ", session['session_id_equipe_materiel_edit'])

            # Récupère la liste des materiel qui ne sont pas associés au equipe sélectionné.
            old_lst_data_equipe_materiel_non_attribues = session['session_lst_data_equipe_materiel_non_attribues']
            print("old_lst_data_equipe_materiel_non_attribues ", old_lst_data_equipe_materiel_non_attribues)

            # Récupère la liste des materiel qui sont associés au equipe sélectionné.
            old_lst_data_equipe_materiel_attribues = session['session_lst_data_equipe_materiel_old_attribues']
            print("old_lst_data_equipe_materiel_old_attribues ", old_lst_data_equipe_materiel_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme materiel dans le composant "tags-selector-tagselect"
            # dans le fichier "equipe_materiel_modifier_tags_dropbox.html"
            new_lst_str_equipe_materiel = request.form.getlist('name_select_tags')
            print("new_lst_str_equipe_materiel ", new_lst_str_equipe_materiel)

            # OM 2020.04.29 Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_equipe_materiel_old = list(map(int, new_lst_str_equipe_materiel))
            print("new_lst_equipe_materiel ", new_lst_int_equipe_materiel_old, "type new_lst_equipe_materiel ",
                  type(new_lst_int_equipe_materiel_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2020.04.29 Une liste de "id_materiel" qui doivent être effacés de la table intermédiaire "t_equipe_materiel".
            lst_diff_materiel_delete_b = list(
                set(old_lst_data_equipe_materiel_attribues) - set(new_lst_int_equipe_materiel_old))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_materiel_delete_b ", lst_diff_materiel_delete_b)

            # OM 2020.04.29 Une liste de "id_materiel" qui doivent être ajoutés à la BD
            lst_diff_materiel_insert_a = list(
                set(new_lst_int_equipe_materiel_old) - set(old_lst_data_equipe_materiel_attribues))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_materiel_insert_a ", lst_diff_materiel_insert_a)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_materiel = Gestionequipemateriel()

            # Pour le equipe sélectionné, parcourir la liste des materiel à INSÉRER dans la "t_equipe_materiel".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_materiel_ins in lst_diff_materiel_insert_a:
                # Constitution d'un dictionnaire pour associer l'id du equipe sélectionné avec un nom de variable
                # et "id_materiel_ins" (l'id du materiel dans la liste) associé à une variable.
                valeurs_equipe_sel_materiel_sel_dictionnaire = {"value_FK_equipe": id_equipe_selected,
                                                           "value_FK_materiel": id_materiel_ins}
                # Insérer une association entre un(des) materiel(s) et le equipe sélectionner.
                obj_actions_materiel.equipe_materiel_add(valeurs_equipe_sel_materiel_sel_dictionnaire)

            # Pour le equipe sélectionné, parcourir la liste des materiel à EFFACER dans la "t_equipe_materiel".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_materiel_del in lst_diff_materiel_delete_b:
                # Constitution d'un dictionnaire pour associer l'id du equipe sélectionné avec un nom de variable
                # et "id_materiel_del" (l'id du materiel dans la liste) associé à une variable.
                valeurs_equipe_sel_materiel_sel_dictionnaire = {"value_FK_equipe": id_equipe_selected,
                                                           "value_FK_materiel": id_materiel_del}
                # Effacer une association entre un(des) materiel(s) et le equipe sélectionner.
                obj_actions_materiel.equipe_materiel_delete(valeurs_equipe_sel_materiel_sel_dictionnaire)

            # Récupère les données grâce à une requête MySql définie dans la classe Gestionmateriel()
            # Fichier data_gestion_materiel.py
            # Afficher seulement le equipe dont les materiel sont modifiés, ainsi l'utilisateur voit directement
            # les changements qu'il a demandés.
            data_equipe_materiel_afficher_concat = obj_actions_materiel.equipe_materiel_afficher_data_concat(id_equipe_selected)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data materiel", data_equipe_materiel_afficher_concat, "type ", type(data_equipe_materiel_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_equipe_materiel_afficher_concat == None:
                flash(f"""Le equipe demandé n'existe pas. Ou la table "t_equipe_materiel" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données materiel affichées dans materielequipe!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Après cette mise à jour de la table intermédiaire "t_equipe_materiel",
    # on affiche les equipe et le(urs) materiel(s) associé(s).
    return render_template("equipe_materiel/equipe_materiel_afficher.html",
                           data=data_equipe_materiel_afficher_concat)
