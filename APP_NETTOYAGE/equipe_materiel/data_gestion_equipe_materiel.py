# data_gestion_equipe_materiel.py
# OM 2020.04.22 Permet de gérer (CRUD) les données de la table intermédiaire "t_equipe_materiel"

from flask import flash
from APP_NETTOYAGE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_NETTOYAGE.DATABASE.erreurs import *


class Gestionequipemateriel():
    def __init__ (self):
        try:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print("dans le try de gestions materiel")
            # OM 2020.04.11 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion materiel equipe ...terrible erreur, il faut connecter une base de donnée", "danger")
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur Gestionequipemateriel {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur Gestionequipemateriel ")

    def materiel_afficher_data (self):
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_materiel"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher
            strsql_materiel_afficher = """SELECT id_materiel, produit_materiel, marque_materiel FROM t_materiel ORDER BY id_materiel ASC"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_materiel_afficher)
                # Récupère les données de la requête.
                data_materiel = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_materiel ", data_materiel, " Type : ", type(data_materiel))
                # Retourne les données du "SELECT"
                return data_materiel
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def equipe_materiel_afficher_data (self, valeur_id_equipe_selected_dict):
        print("valeur_id_equipe_selected_dict8..", valeur_id_equipe_selected_dict)
        try:

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_materiel"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_equipe_selected = """SELECT id_equipe, nom_equipe, GROUP_CONCAT(id_materiel) as materielequipe FROM t_equipe_materiel AS T1
                                        INNER JOIN t_equipe AS T2 ON T2.id_equipe = T1.FK_equipe
                                        INNER JOIN t_materiel AS T3 ON T3.id_materiel = T1.FK_materiel
                                        WHERE id_equipe = %(value_id_equipe_selected)s"""

            strsql_equipe_materiel_non_attribues = """SELECT id_materiel, produit_materiel, marque_materiel FROM t_materiel
                                                    WHERE id_materiel not in(SELECT id_materiel as idmaterielequipe FROM t_equipe_materiel AS T1
                                                    INNER JOIN t_equipe AS T2 ON T2.id_equipe = T1.FK_equipe
                                                    INNER JOIN t_materiel AS T3 ON T3.id_materiel = T1.FK_materiel
                                                    WHERE id_equipe = %(value_id_equipe_selected)s)"""

            strsql_equipe_materiel_attribues = """SELECT id_equipe, id_materiel, produit_materiel, marque_materiel FROM t_equipe_materiel AS T1
                                            INNER JOIN t_equipe AS T2 ON T2.id_equipe = T1.FK_equipe
                                            INNER JOIN t_materiel AS T3 ON T3.id_materiel = T1.FK_materiel
                                            WHERE id_equipe = %(value_id_equipe_selected)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_equipe_materiel_non_attribues, valeur_id_equipe_selected_dict)
                # Récupère les données de la requête.
                data_equipe_materiel_non_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("dfad data_equipe_materiel_non_attribues ", data_equipe_materiel_non_attribues, " Type : ",
                      type(data_equipe_materiel_non_attribues))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_equipe_selected, valeur_id_equipe_selected_dict)
                # Récupère les données de la requête.
                data_equipe_selected = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_equipe_selected  ", data_equipe_selected, " Type : ", type(data_equipe_selected))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_equipe_materiel_attribues, valeur_id_equipe_selected_dict)
                # Récupère les données de la requête.
                data_equipe_materiel_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_equipe_materiel_attribues ", data_equipe_materiel_attribues, " Type : ",
                      type(data_equipe_materiel_attribues))

                # Retourne les données du "SELECT"
                return data_equipe_selected, data_equipe_materiel_non_attribues, data_equipe_materiel_attribues
        except pymysql.Error as erreur:
            print(f"DGGF gfad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def equipe_materiel_afficher_data_concat (self, id_equipe_selected):
        print("id_equipe_selected  ", id_equipe_selected)
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_materiel"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_equipe_materiel_afficher_data_concat = """SELECT id_equipe, nom_equipe,
                                                            GROUP_CONCAT(produit_materiel) as materielequipe FROM t_equipe_materiel AS T1
                                                            RIGHT JOIN t_equipe AS T2 ON T2.id_equipe = T1.FK_equipe
                                                            LEFT JOIN t_materiel AS T3 ON T3.id_materiel = T1.FK_materiel
                                                            GROUP BY id_equipe"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # le paramètre 0 permet d'afficher tous les equipe
                # Sinon le paramètre représente la valeur de l'id du equipe
                if id_equipe_selected == 0:
                    mc_afficher.execute(strsql_equipe_materiel_afficher_data_concat)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du equipe sélectionné avec un nom de variable
                    valeur_id_equipe_selected_dictionnaire = {"value_id_equipe_selected": id_equipe_selected}
                    strsql_equipe_materiel_afficher_data_concat += """ HAVING id_equipe= %(value_id_equipe_selected)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_equipe_materiel_afficher_data_concat, valeur_id_equipe_selected_dictionnaire)

                # Récupère les données de la requête.
                data_equipe_materiel_afficher_concat = mc_afficher.fetchall()
                # Affichage dans la console
                print("dggf data_equipe_materiel_afficher_concat ", data_equipe_materiel_afficher_concat, " Type : ",
                      type(data_equipe_materiel_afficher_concat))

                # Retourne les données du "SELECT"
                return data_equipe_materiel_afficher_concat


        except pymysql.Error as erreur:
            print(f"DGGF gfadc pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfadc Exception {erreur.args}")
            raise MaBdErreurConnexion(
                f"DGG gfadc Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfadc pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def equipe_materiel_add (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_equipe" et "id_materiel" dans la "t_equipe_materiel"
            strsql_insert_equipe_materiel = """INSERT INTO t_equipe_materiel (id_equipe_materiel, FK_materiel, FK_equipe)
                                            VALUES (NULL, %(value_FK_materiel)s, %(value_FK_equipe)s)"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_equipe_materiel, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

    def equipe_materiel_delete (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Effacer une (des) association(s) existantes entre "id_equipe" et "id_materiel" dans la "t_equipe_materiel"
            strsql_delete_equipe_materiel = """DELETE FROM t_equipe_materiel WHERE FK_materiel = %(value_FK_materiel)s AND FK_equipe = %(value_FK_equipe)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_delete_equipe_materiel, valeurs_insertion_dictionnaire)
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème equipe_materiel_delete Gestions materiel equipe numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème equipe_materiel_delete Gestions materiel equipe  numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème equipe_materiel_delete Gestions materiel equipe  {erreur}")

    def edit_materiel_data (self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le materiel sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_equipes = "SELECT id_materiel, produit_materiel FROM t_materiel WHERE id_materiel = %(value_id_materiel)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_equipes, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_materiel_data Data Gestions materiel numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions materiel numéro de l'erreur : {erreur}", "danger")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_materiel_data d'un materiel Data Gestions materiel {erreur}")

    def update_materiel_data (self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditnommaterielHTML" du form HTML "materielEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnommaterielHTML" value="{{ row.nom_materiel }}"/></td>
            str_sql_update_nommateriel = "UPDATE t_materiel SET produit_materiel, marque_materiel = %(value_name_genre)s WHERE id_materiel = %(value_id_materiel)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_nommateriel, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_materiel_data Data Gestions materiel numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions materiel numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_materiel_data d\'un materiel Data Gestions materiel {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "warning")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash(f"Doublon !!! Introduire une valeur différente", "warning")
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_materiel_data Data Gestions materiel numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_materiel_data d'un materiel DataGestionsmateriel {erreur}")

    def delete_select_materiel_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditnommaterielHTML" du form HTML "materielEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnommaterielHTML" value="{{ row.nom_materiel }}"/></td>

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le materiel sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_materiel = "SELECT id_materiel, produit_materiel, marque_materiel FROM t_materiel WHERE id_materiel = %(value_id_materiel)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode"mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_materiel, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_select_materiel_data Gestions materiel numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_materiel_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_materiel_data d\'un materiel Data Gestions materiel {erreur}")

    def delete_materiel_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "materielEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnommaterielHTML" value="{{ row.nom_materiel }}"/></td>
            str_sql_delete_nommateriel = "DELETE FROM t_materiel WHERE id_materiel = %(value_id_materiel)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_nommateriel, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_materiel_data Data Gestions materiel numéro de l'erreur : {erreur}")
            flash(f"Flash. Problèmes Data Gestions materiel numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # OM 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un materiel qui est associé à un equipe dans la table intermédiaire "t_equipe_materiel"
                # il y a une contrainte sur les FK de la table intermédiaire "t_equipe_materiel"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce materiel est associé à des equipe dans la t_equipe_materiel !!! : {erreur}", "danger")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !!! Ce materiel est associé à des equipe dans la t_equipe_materiel !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")
