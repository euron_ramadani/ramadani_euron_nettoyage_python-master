# routes_gestion_personne_adresse.py
# OM 2020.04.16 Gestions des "routes" FLASK pour la table intermédiaire qui associe les personne et les adresse.

from flask import render_template, request, flash, session
from APP_NETTOYAGE import obj_mon_application
from APP_NETTOYAGE.adresse.data_gestion_adresse import Gestionadresse
from APP_NETTOYAGE.personne_adresse.data_gestion_personne_adresse import Gestionpersonneadresse


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /personne_adresse_afficher_concat
# Récupère la liste de tous les personne et de tous les adresse associés aux personne.
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/personne_adresse_afficher_concat/<int:id_personne_sel>", methods=['GET', 'POST'])
def personne_adresse_afficher_concat (id_personne_sel):
    print("id_personne_sel ", id_personne_sel)
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = Gestionpersonneadresse()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionadresse()
            # Fichier data_gestion_adresse.py
            data_personne_adresse_afficher_concat = obj_actions_adresse.personne_adresse_afficher_data_concat(id_personne_sel)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data adresse", data_personne_adresse_afficher_concat, "type ", type(data_personne_adresse_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_personne_adresse_afficher_concat:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données adresse affichés dans adressepersonne!!", "success")
            else:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_personne_adresse" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("personne_adresse/personne_adresse_afficher.html",
                           data=data_personne_adresse_afficher_concat)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.21 Définition d'une "route" /gf_edit_personne_adresse_selected
# Récupère la liste de tous les adresse du personne sélectionné.
# Nécessaire pour afficher tous les "TAGS" des adresse, ainsi l'utilisateur voit les adresse à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_edit_personne_adresse_selected", methods=['GET', 'POST'])
def gf_edit_personne_adresse_selected ():
    if request.method == "GET":
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = Gestionadresse()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionadresse()
            # Fichier data_gestion_adresse.py
            # Pour savoir si la table "t_adresse" est vide, ainsi on empêche l’affichage des tags
            # dans le render_template(personne_adresse_modifier_tags_dropbox.html)
            data_adresse_all = obj_actions_adresse.adresse_afficher_data()

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données de la table intermédiaire.
            obj_actions_adresse = Gestionpersonneadresse()

            # OM 2020.04.21 Récupère la valeur de "id_personne" du formulaire html "personne_adresse_afficher.html"
            # l'utilisateur clique sur le lien "Modifier adresse de ce personne" et on récupère la valeur de "id_personne" grâce à la variable "id_personne_adresse_edit_html"
            # <a href="{{ url_for('gf_edit_personne_adresse_selected', id_personne_adresse_edit_html=row.id_personne) }}">Modifier les adresse de ce personne</a>
            id_personne_adresse_edit = request.values['id_personne_adresse_edit_html']

            # OM 2020.04.21 Mémorise l'id du personne dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_personne_adresse_edit'] = id_personne_adresse_edit

            # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
            valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_personne_adresse_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la classe Gestionpersonneadresse()
            # 1) Sélection du personne choisi
            # 2) Sélection des adresse "déjà" attribués pour le personne.
            # 3) Sélection des adresse "pas encore" attribués pour le personne choisi.
            # Fichier data_gestion_personne_adresse.py
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "personne_adresse_afficher_data"
            data_personne_adresse_selected, data_personne_adresse_non_attribues, data_personne_adresse_attribues = \
                obj_actions_adresse.personne_adresse_afficher_data(valeur_id_personne_selected_dictionnaire)

            lst_data_personne_selected = [item['id_personne'] for item in data_personne_adresse_selected]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_selected  ", lst_data_personne_selected,
                  type(lst_data_personne_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les adresse qui ne sont pas encore sélectionnés.
            lst_data_personne_adresse_non_attribues = [item['id_adresse'] for item in data_personne_adresse_non_attribues]
            session['session_lst_data_personne_adresse_non_attribues'] = lst_data_personne_adresse_non_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_adresse_non_attribues  ", lst_data_personne_adresse_non_attribues,
                  type(lst_data_personne_adresse_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les adresse qui sont déjà sélectionnés.
            lst_data_personne_adresse_old_attribues = [item['id_adresse'] for item in data_personne_adresse_attribues]
            session['session_lst_data_personne_adresse_old_attribues'] = lst_data_personne_adresse_old_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_adresse_old_attribues  ", lst_data_personne_adresse_old_attribues,
                  type(lst_data_personne_adresse_old_attribues))

            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data data_personne_adresse_selected", data_personne_adresse_selected, "type ", type(data_personne_adresse_selected))
            print(" data data_personne_adresse_non_attribues ", data_personne_adresse_non_attribues, "type ",
                  type(data_personne_adresse_non_attribues))
            print(" data_personne_adresse_attribues ", data_personne_adresse_attribues, "type ",
                  type(data_personne_adresse_attribues))

            # Extrait les valeurs contenues dans la table "t_adresse", colonne "nom_adresse"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_adresse
            lst_data_personne_adresse_non_attribues = [item['adresse'] for item in data_personne_adresse_non_attribues]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_all_adresse gf_edit_personne_adresse_selected ", lst_data_personne_adresse_non_attribues,
                  type(lst_data_personne_adresse_non_attribues))

            # Différencier les messages si la table est vide.
            if lst_data_personne_selected == [None]:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_personne_adresse" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données adresse affichées dans adressepersonne!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("personne_adresse/personne_adresse_modifier_tags_dropbox.html",
                           data_adresse=data_adresse_all,
                           data_personne_selected=data_personne_adresse_selected,
                           data_adresse_attribues=data_personne_adresse_attribues,
                           data_adresse_non_attribues=data_personne_adresse_non_attribues)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /gf_update_personne_adresse_selected
# Récupère la liste de tous les adresse du personne sélectionné.
# Nécessaire pour afficher tous les "TAGS" des adresse, ainsi l'utilisateur voit les adresse à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_update_personne_adresse_selected", methods=['GET', 'POST'])
def gf_update_personne_adresse_selected ():
    if request.method == "POST":
        try:
            # Récupère l'id du personne sélectionné
            id_personne_selected = session['session_id_personne_adresse_edit']
            print("session['session_id_personne_adresse_edit'] ", session['session_id_personne_adresse_edit'])

            # Récupère la liste des adresse qui ne sont pas associés au personne sélectionné.
            old_lst_data_personne_adresse_non_attribues = session['session_lst_data_personne_adresse_non_attribues']
            print("old_lst_data_personne_adresse_non_attribues ", old_lst_data_personne_adresse_non_attribues)

            # Récupère la liste des adresse qui sont associés au personne sélectionné.
            old_lst_data_personne_adresse_attribues = session['session_lst_data_personne_adresse_old_attribues']
            print("old_lst_data_personne_adresse_old_attribues ", old_lst_data_personne_adresse_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme adresse dans le composant "tags-selector-tagselect"
            # dans le fichier "personne_adresse_modifier_tags_dropbox.html"
            new_lst_str_personne_adresse = request.form.getlist('name_select_tags')
            print("new_lst_str_personne_adresse ", new_lst_str_personne_adresse)

            # OM 2020.04.29 Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_personne_adresse_old = list(map(int, new_lst_str_personne_adresse))
            print("new_lst_personne_adresse ", new_lst_int_personne_adresse_old, "type new_lst_personne_adresse ",
                  type(new_lst_int_personne_adresse_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2020.04.29 Une liste de "id_adresse" qui doivent être effacés de la table intermédiaire "t_personne_adresse".
            lst_diff_adresse_delete_b = list(
                set(old_lst_data_personne_adresse_attribues) - set(new_lst_int_personne_adresse_old))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_adresse_delete_b ", lst_diff_adresse_delete_b)

            # OM 2020.04.29 Une liste de "id_adresse" qui doivent être ajoutés à la BD
            lst_diff_adresse_insert_a = list(
                set(new_lst_int_personne_adresse_old) - set(old_lst_data_personne_adresse_attribues))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_adresse_insert_a ", lst_diff_adresse_insert_a)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = Gestionpersonneadresse()

            # Pour le personne sélectionné, parcourir la liste des adresse à INSÉRER dans la "t_personne_adresse".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_adresse_ins in lst_diff_adresse_insert_a:
                # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                # et "id_adresse_ins" (l'id du adresse dans la liste) associé à une variable.
                valeurs_personne_sel_adresse_sel_dictionnaire = {"value_FK_personne": id_personne_selected,
                                                           "value_FK_adresse": id_adresse_ins}
                # Insérer une association entre un(des) adresse(s) et le personne sélectionner.
                obj_actions_adresse.personne_adresse_add(valeurs_personne_sel_adresse_sel_dictionnaire)

            # Pour le personne sélectionné, parcourir la liste des adresse à EFFACER dans la "t_personne_adresse".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_adresse_del in lst_diff_adresse_delete_b:
                # Constitution d'un dictionnaire pour associer l'id du personne sélectionné avec un nom de variable
                # et "id_adresse_del" (l'id du adresse dans la liste) associé à une variable.
                valeurs_personne_sel_adresse_sel_dictionnaire = {"value_FK_personne": id_personne_selected,
                                                           "value_FK_adresse": id_adresse_del}
                # Effacer une association entre un(des) adresse(s) et le personne sélectionner.
                obj_actions_adresse.personne_adresse_delete(valeurs_personne_sel_adresse_sel_dictionnaire)

            # Récupère les données grâce à une requête MySql définie dans la classe Gestionadresse()
            # Fichier data_gestion_adresse.py
            # Afficher seulement le personne dont les adresse sont modifiés, ainsi l'utilisateur voit directement
            # les changements qu'il a demandés.
            data_personne_adresse_afficher_concat = obj_actions_adresse.personne_adresse_afficher_data_concat(id_personne_selected)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data adresse", data_personne_adresse_afficher_concat, "type ", type(data_personne_adresse_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_personne_adresse_afficher_concat == None:
                flash(f"""Le personne demandé n'existe pas. Ou la table "t_personne_adresse" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données adresse affichées dans adressepersonne!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Après cette mise à jour de la table intermédiaire "t_personne_adresse",
    # on affiche les personne et le(urs) adresse(s) associé(s).
    return render_template("personne_adresse/personne_adresse_afficher.html",
                           data=data_personne_adresse_afficher_concat)
