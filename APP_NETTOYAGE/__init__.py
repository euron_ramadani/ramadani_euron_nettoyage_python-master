# Un objet "obj_mon_application" pour utiliser la classe Flask
# Pour les batiment qui veulent savoir ce que signifie __name__ une démonstration se trouve ici :
# https://www.studytonight.com/python/_name_-as-main-method-in-python
# __name__ garantit que la méthode run() est appelée uniquement lorsque main.py est exécuté en tant que programme principal.
# La méthode run() ne sera pas appelée si vous importez main.py dans un autre module Python.
from flask import Flask
from APP_NETTOYAGE.DATABASE import connect_db_context_manager


# Objet qui fait "exister" notre application
obj_mon_application = Flask(__name__, template_folder="templates")
# Flask va pouvoir crypter les cookies
obj_mon_application.secret_key = '_vogonAmiral_)?^'

# Doit se trouver ici... soit après l'instanciation de la classe "Flask"
# OM 2020.03.25 Tout commence ici par "indiquer" les routes de l'application.
from APP_NETTOYAGE import routes
from APP_NETTOYAGE.adresse import routes_gestion_adresse
from APP_NETTOYAGE.batiment import routes_gestion_batiment
from APP_NETTOYAGE.adresse_batiment import routes_gestion_adresse_batiment
from APP_NETTOYAGE.equipe import routes_gestion_equipe
from APP_NETTOYAGE.equipe_batiment import routes_gestion_equipe_batiment
from APP_NETTOYAGE.materiel import routes_gestion_materiel
from APP_NETTOYAGE.equipe_materiel import routes_gestion_equipe_materiel
from APP_NETTOYAGE.personne import routes_gestion_personne
from APP_NETTOYAGE.personne_adresse import routes_gestion_personne_adresse
from APP_NETTOYAGE.mail import routes_gestion_mail
from APP_NETTOYAGE.personne_mail import routes_gestion_personne_mail
from APP_NETTOYAGE.personne_equipe import routes_gestion_personne_equipe
from APP_NETTOYAGE.telephone import routes_gestion_telephone
from APP_NETTOYAGE.personne_telephone import routes_gestion_personne_telephone