# routes_gestion_adresse.py
# OM 2020.04.06 Gestions des "routes" FLASK pour les adresse.

import pymysql
from flask import render_template, flash, redirect, url_for, request
from APP_NETTOYAGE import obj_mon_application
from APP_NETTOYAGE.adresse.data_gestion_adresse import Gestionadresse
from APP_NETTOYAGE.DATABASE.erreurs import *
import re

@obj_mon_application.route("/adresse_afficher")
def adresse_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = Gestionadresse()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionadresse()
            # Fichier data_gestion_adresse.py
            data_adresse = obj_actions_adresse.adresse_afficher_data()
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data adresse", data_adresse, "type ", type(data_adresse))
            # Différencier les messages si la table est vide.
            if data_adresse:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données Objet affichées !!", "success")
            else:
                flash("""La table "t_adresse" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}","danger")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("adresse/adresse_afficher.html", data=data_adresse)


# OM 2020.04.06 Pour une simple démo. On insère deux fois des valeurs dans la table adresse
# Une fois de manière fixe, vous devez changer les valeurs pour voir le résultat dans la table "t_adresse"
# La 2ème il faut entrer la valeur du titre du adresse par le clavier, il ne doit pas être vide.
# Pour les autres valeurs elles doivent être changées ci-dessous.
# Une des valeurs est "None" ce qui en MySql donne "NULL" pour l'attribut "t_adresse.cover_link_adresse"
# Pour la tester http://127.0.0.1:5005/adresse_add
@obj_mon_application.route("/adresse_add", methods=['GET', 'POST'])
def adresse_add ():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = Gestionadresse()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "adresse_add.html"
            adresse = request.form['adresse_html']
            ville = request.form['ville_html']
            NPA_adresse = request.form['NPA_adresse_html']
            print("Ici ok")
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            # if not re.match("^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$",
            #                 adresse):
            #     print("Ici OK")
            #     # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
            #     flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
            #           f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")
            #     # On doit afficher à nouveau le formulaire "adresse_add.html" à cause des erreurs de "claviotage"
            #     return render_template("adresse/adresse_add.html")
            # else:

            # Constitution d'un dictionnaire et insertion dans la BD
            valeurs_insertion_dictionnaire = {"value_adresse": adresse,
                                              "value_ville": ville,
                                              "value_NPA_adresse": NPA_adresse}
            obj_actions_adresse.add_adresse_data(valeurs_insertion_dictionnaire)

            # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Données insérées !!", "success")
            print(f"Données insérées !!")
            # On va interpréter la "route" 'adresse_afficher', car l'utilisateur
            # doit voir le nouveau adresse qu'il vient d'insérer. Et on l'affiche de manière
            # à voir le dernier élément inséré.
            return redirect(url_for('adresse_afficher', order_by = 'DESC', id_adresse_sel=0))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("adresse/adresse_add.html")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /adresse_edit ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un adresse de adresse par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/adresse_edit', methods=['POST', 'GET'])
def adresse_edit ():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "adresse_afficher.html"
    if request.method == 'GET':
        try:
            # Récupère la valeur de "id_adresse" du formulaire html "adresse_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_adresse"
            # grâce à la variable "id_adresse_edit_html"
            # <a href="{{ url_for('adresse_edit', id_adresse_edit_html=row.id_adresse) }}">Edit</a>
            id_adresse_edit = request.values['id_adresse_edit_html']

            # Pour afficher dans la console la valeur de "id_adresse_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(id_adresse_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_adresse": id_adresse_edit}

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = Gestionadresse()

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_adresse = obj_actions_adresse.edit_adresse_data(valeur_select_dictionnaire)
            print("dataidadresse ", data_id_adresse, "type ", type(data_id_adresse))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le adresse d'un adresse !!!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("adresse/adresse_edit.html", data=data_id_adresse)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /adresse_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un adresse de adresse par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/adresse_update', methods=['POST', 'GET'])
def adresse_update ():
    # DEBUG bon marché : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "adresse_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du adresse alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # DEBUG bon marché : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ", request.values)

            # Récupère la valeur de "id_adresse" du formulaire html "adresse_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_adresse"
            # grâce à la variable "id_adresse_edit_html"
            # <a href="{{ url_for('adresse_edit', id_adresse_edit_html=row.id_adresse) }}">Edit</a>
            id_adresse_edit = request.values['id_adresse_edit_html']

            # Récupère le contenu du champ "adresse" dans le formulaire HTML "adresseEdit.html"
            adresse = request.values['edit_adresse_html']
            ville = request.values['edit_ville_html']
            NPA_adresse = request.values['edit_NPA_adresse_html']
            valeur_edit_list = [{'id_adresse': id_adresse_edit, 'adresse': adresse
                                 ,'ville': ville,'NPA_adresse': NPA_adresse}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$",
                            adresse):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "adresse" dans le formulaire HTML "adresseEdit.html"
                # adresse = request.values['name_edit_adresse_html']
                # Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")

                # On doit afficher à nouveau le formulaire "adresse_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "adresse_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_adresse': 13, 'adresse': 'philosophique'}]
                valeur_edit_list = [{'id_adresse': id_adresse_edit, 'adresse': adresse
                                     ,'ville': ville,'NPA_adresse': NPA_adresse}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "adresse_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('adresse/adresse_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_adresse": id_adresse_edit, "value_adresse": adresse,
                                              "value_ville": ville, "value_NPA_adresse": NPA_adresse
                                              }

                # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_adresse = Gestionadresse()

                # La commande MySql est envoyée à la BD
                data_id_adresse = obj_actions_adresse.update_adresse_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataidadresse ", data_id_adresse, "type ", type(data_id_adresse))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Valeur adresse modifiée. ", "success")
                # On affiche les adresse avec celui qui vient d'être edité en tête de liste. (DESC)
                return redirect(url_for('adresse_afficher', order_by="ASC", id_adresse_sel=id_adresse_edit))

        except (Exception,
                # pymysql.err.OperationalError,
                # pymysql.ProgrammingError,
                # pymysql.InternalError,
                # pymysql.IntegrityError,
                TypeError) as erreur:
            print(erreur.args[0])
            flash(f"problème adresse ____lllupdate{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_adresse_html" alors on renvoie le formulaire "EDIT"
    return render_template('adresse/adresse_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /adresse_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un adresse de adresse par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/adresse_select_delete', methods=['POST', 'GET'])
def adresse_select_delete ():
    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = Gestionadresse()
            # OM 2019.04.04 Récupère la valeur de "idadresseDeleteHTML" du formulaire html "adresseDelete.html"
            id_adresse_delete = request.args.get('id_adresse_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_adresse": id_adresse_delete}

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_adresse = obj_actions_adresse.delete_select_adresse_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur adresse_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur adresse_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('adresse/adresse_delete.html', data=data_id_adresse)


# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /adresseUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un adresse, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/adresse_delete', methods=['POST', 'GET'])
def adresse_delete ():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = Gestionadresse()
            # OM 2019.04.02 Récupère la valeur de "id_adresse" du formulaire html "adresseAfficher.html"
            id_adresse_delete = request.form['id_adresse_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_adresse": id_adresse_delete}

            data_adresse = obj_actions_adresse.delete_adresse_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des adresse des adresse
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les adresse
            return redirect(url_for('adresse_afficher',order_by="ASC",id_adresse_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "adresse" de adresse qui est associé dans "t_adresse_adresse".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des adresse !', "warning")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce adresse est associé à des adresse dans la t_adresse_adresse !!! : {erreur}")
                # Afficher la liste des adresse des adresse
                return redirect(url_for('adresse_afficher', order_by="ASC", id_adresse_sel=0))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur adresse_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur adresse_delete {erreur.args[0], erreur.args[1]}", "danger")

            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('adresse/adresse_afficher.html', data=data_adresse)

