# routes_gestion_equipe.py
# OM 2020.04.06 Gestions des "routes" FLASK pour les equipe.

from flask import render_template, flash, redirect, url_for, request
from APP_NETTOYAGE import obj_mon_application
from APP_NETTOYAGE.equipe.data_gestion_equipe import Gestionequipe
from APP_NETTOYAGE.DATABASE.erreurs import *
import re

@obj_mon_application.route("/equipe_afficher")
def equipe_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_equipe = Gestionequipe()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionequipe()
            # Fichier data_gestion_equipe.py
            data_equipe = obj_actions_equipe.equipe_afficher_data()
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data equipe", data_equipe, "type ", type(data_equipe))
            # Différencier les messages si la table est vide.
            if data_equipe:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données Objet affichées !!", "success")
            else:
                flash("""La table "t_equipe" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}","danger")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("equipe/equipe_afficher.html", data=data_equipe)


# OM 2020.04.06 Pour une simple démo. On insère deux fois des valeurs dans la table equipe
# Une fois de manière fixe, vous devez changer les valeurs pour voir le résultat dans la table "t_equipe"
# La 2ème il faut entrer la valeur du titre du equipe par le clavier, il ne doit pas être vide.
# Pour les autres valeurs elles doivent être changées ci-dessous.
# Une des valeurs est "None" ce qui en MySql donne "NULL" pour l'attribut "t_equipe.cover_link_equipe"
# Pour la tester http://127.0.0.1:5005/equipe_add
@obj_mon_application.route("/equipe_add", methods=['GET', 'POST'])
def equipe_add ():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_equipe = Gestionequipe()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "equipe_add.html"
            nom_equipe = request.form['nom_equipe_html']
            print("Ici ok")
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            # if not re.match("^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$",
            #                 equipe):
            #     print("Ici OK")
            #     # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
            #     flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
            #           f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")
            #     # On doit afficher à nouveau le formulaire "equipe_add.html" à cause des erreurs de "claviotage"
            #     return render_template("equipe/equipe_add.html")
            # else:

            # Constitution d'un dictionnaire et insertion dans la BD
            valeurs_insertion_dictionnaire = {"value_nom_equipe": nom_equipe}
            obj_actions_equipe.add_equipe_data(valeurs_insertion_dictionnaire)

            # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Données insérées !!", "success")
            print(f"Données insérées !!")
            # On va interpréter la "route" 'equipe_afficher', car l'utilisateur
            # doit voir le nouveau equipe qu'il vient d'insérer. Et on l'affiche de manière
            # à voir le dernier élément inséré.
            return redirect(url_for('equipe_afficher', order_by = 'DESC', id_equipe_sel=0))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("equipe/equipe_add.html")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /equipe_edit ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un equipe de equipe par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/equipe_edit', methods=['POST', 'GET'])
def equipe_edit ():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "equipe_afficher.html"
    if request.method == 'GET':
        try:
            # Récupère la valeur de "id_equipe" du formulaire html "equipe_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_equipe"
            # grâce à la variable "id_equipe_edit_html"
            # <a href="{{ url_for('equipe_edit', id_equipe_edit_html=row.id_equipe) }}">Edit</a>
            id_equipe_edit = request.values['id_equipe_edit_html']

            # Pour afficher dans la console la valeur de "id_equipe_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(id_equipe_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_equipe": id_equipe_edit}

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_equipe = Gestionequipe()

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_equipe = obj_actions_equipe.edit_equipe_data(valeur_select_dictionnaire)
            print("dataidequipe ", data_id_equipe, "type ", type(data_id_equipe))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le equipe d'un equipe !!!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("equipe/equipe_edit.html", data=data_id_equipe)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /equipe_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un equipe de equipe par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/equipe_update', methods=['POST', 'GET'])
def equipe_update ():
    # DEBUG bon marché : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "equipe_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du equipe alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # DEBUG bon marché : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ", request.values)

            # Récupère la valeur de "id_equipe" du formulaire html "equipe_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_equipe"
            # grâce à la variable "id_equipe_edit_html"
            # <a href="{{ url_for('equipe_edit', id_equipe_edit_html=row.id_equipe) }}">Edit</a>
            id_equipe_edit = request.values['id_equipe_edit_html']

            # Récupère le contenu du champ "equipe" dans le formulaire HTML "equipeEdit.html"
            nom_equipe = request.values['edit_nom_equipe_html']
            valeur_edit_list = [{'id_equipe': id_equipe_edit, 'nom_equipe': nom_equipe}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$",
                            nom_equipe):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "equipe" dans le formulaire HTML "equipeEdit.html"
                # equipe = request.values['name_edit_equipe_html']
                # Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")

                # On doit afficher à nouveau le formulaire "equipe_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "equipe_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_equipe': 13, 'equipe': 'philosophique'}]
                valeur_edit_list = [{'id_equipe': id_equipe_edit, 'nom_equipe': nom_equipe}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "equipe_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('equipe/equipe_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_equipe": id_equipe_edit, "value_nom_equipe": nom_equipe}

                # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_equipe = Gestionequipe()

                # La commande MySql est envoyée à la BD
                data_id_equipe = obj_actions_equipe.update_equipe_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataidequipe ", data_id_equipe, "type ", type(data_id_equipe))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Valeur equipe modifiée. ", "success")
                # On affiche les equipe avec celui qui vient d'être edité en tête de liste. (DESC)
                return redirect(url_for('equipe_afficher', order_by="ASC", id_equipe_sel=id_equipe_edit))

        except (Exception,
                # pymysql.err.OperationalError,
                # pymysql.ProgrammingError,
                # pymysql.InternalError,
                # pymysql.IntegrityError,
                TypeError) as erreur:
            print(erreur.args[0])
            flash(f"problème equipe ____lllupdate{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_equipe_html" alors on renvoie le formulaire "EDIT"
    return render_template('equipe/equipe_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /equipe_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un equipe de equipe par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/equipe_select_delete', methods=['POST', 'GET'])
def equipe_select_delete ():
    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_equipe = Gestionequipe()
            # OM 2019.04.04 Récupère la valeur de "idequipeDeleteHTML" du formulaire html "equipeDelete.html"
            id_equipe_delete = request.args.get('id_equipe_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_equipe": id_equipe_delete}

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_equipe = obj_actions_equipe.delete_select_equipe_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur equipe_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur equipe_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('equipe/equipe_delete.html', data=data_id_equipe)


# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /equipeUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un equipe, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/equipe_delete', methods=['POST', 'GET'])
def equipe_delete ():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_equipe = Gestionequipe()
            # OM 2019.04.02 Récupère la valeur de "id_equipe" du formulaire html "equipeAfficher.html"
            id_equipe_delete = request.form['id_equipe_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_equipe": id_equipe_delete}

            data_equipe = obj_actions_equipe.delete_equipe_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des equipe des equipe
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les equipe
            return redirect(url_for('equipe_afficher',order_by="ASC",id_equipe_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "equipe" de equipe qui est associé dans "t_equipe_equipe".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des equipe !', "warning")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce equipe est associé à des equipe dans la t_equipe_equipe !!! : {erreur}")
                # Afficher la liste des equipe des equipe
                return redirect(url_for('equipe_afficher', order_by="ASC", id_equipe_sel=0))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur equipe_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur equipe_delete {erreur.args[0], erreur.args[1]}", "danger")

            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('equipe/equipe_afficher.html', data=data_equipe)

