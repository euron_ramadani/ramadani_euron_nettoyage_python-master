# routes_gestion_mail.py
# OM 2020.04.06 Gestions des "routes" FLASK pour les mail.

import pymysql
from flask import render_template, flash, redirect, url_for, request
from APP_NETTOYAGE import obj_mon_application
from APP_NETTOYAGE.mail.data_gestion_mail import Gestionmail
from APP_NETTOYAGE.DATABASE.erreurs import *
import re

@obj_mon_application.route("/mail_afficher")
def mail_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = Gestionmail()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionmail()
            # Fichier data_gestion_mail.py
            data_mail = obj_actions_mail.mail_afficher_data()
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data mail", data_mail, "type ", type(data_mail))
            # Différencier les messages si la table est vide.
            if data_mail:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données Objet affichées !!", "success")
            else:
                flash("""La table "t_mail" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}","danger")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("mail/mail_afficher.html", data=data_mail)


# OM 2020.04.06 Pour une simple démo. On insère deux fois des valeurs dans la table mail
# Une fois de manière fixe, vous devez changer les valeurs pour voir le résultat dans la table "t_mail"
# La 2ème il faut entrer la valeur du titre du mail par le clavier, il ne doit pas être vide.
# Pour les autres valeurs elles doivent être changées ci-dessous.
# Une des valeurs est "None" ce qui en MySql donne "NULL" pour l'attribut "t_mail.cover_link_mail"
# Pour la tester http://127.0.0.1:5005/mail_add
@obj_mon_application.route("/mail_add", methods=['GET', 'POST'])
def mail_add ():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = Gestionmail()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "mail_add.html"
            adresse_mail = request.form['adresse_mail_html']
            print("Ici ok")
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            # if not re.match("^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$",
            #                 mail):
            #     print("Ici OK")
            #     # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
            #     flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
            #           f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")
            #     # On doit afficher à nouveau le formulaire "mail_add.html" à cause des erreurs de "claviotage"
            #     return render_template("mail/mail_add.html")
            # else:

            # Constitution d'un dictionnaire et insertion dans la BD
            valeurs_insertion_dictionnaire = {"value_adresse_mail": adresse_mail}
            obj_actions_mail.add_mail_data(valeurs_insertion_dictionnaire)

            # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Données insérées !!", "success")
            print(f"Données insérées !!")
            # On va interpréter la "route" 'mail_afficher', car l'utilisateur
            # doit voir le nouveau mail qu'il vient d'insérer. Et on l'affiche de manière
            # à voir le dernier élément inséré.
            return redirect(url_for('mail_afficher', order_by = 'DESC', id_mail_sel=0))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("mail/mail_add.html")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /mail_edit ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un mail de adresse par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/mail_edit', methods=['POST', 'GET'])
def mail_edit ():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "mail_afficher.html"
    if request.method == 'GET':
        try:
            # Récupère la valeur de "id_mail" du formulaire html "mail_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_mail"
            # grâce à la variable "id_mail_edit_html"
            # <a href="{{ url_for('mail_edit', id_mail_edit_html=row.id_mail) }}">Edit</a>
            id_mail_edit = request.values['id_mail_edit_html']

            # Pour afficher dans la console la valeur de "id_mail_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(id_mail_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_mail": id_mail_edit}

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = Gestionmail()

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_mail = obj_actions_mail.edit_mail_data(valeur_select_dictionnaire)
            print("dataidmail ", data_id_mail, "type ", type(data_id_mail))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le mail d'un adresse !!!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("mail/mail_edit.html", data=data_id_mail)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /mail_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un mail de adresse par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/mail_update', methods=['POST', 'GET'])
def mail_update ():
    # DEBUG bon marché : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "mail_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du mail alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # DEBUG bon marché : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ", request.values)

            # Récupère la valeur de "id_mail" du formulaire html "mail_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_mail"
            # grâce à la variable "id_mail_edit_html"
            # <a href="{{ url_for('mail_edit', id_mail_edit_html=row.id_mail) }}">Edit</a>
            id_mail_edit = request.values['id_mail_edit_html']

            # Récupère le contenu du champ "adresse_mail" dans le formulaire HTML "mailEdit.html"
            adresse_mail = request.values['edit_adresse_mail_html']
            valeur_edit_list = [{'id_mail': id_mail_edit, 'adresse_mail': adresse_mail}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",
                            adresse_mail):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "adresse_mail" dans le formulaire HTML "mailEdit.html"
                # adresse_mail = request.values['name_edit_adresse_mail_html']
                # Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, arobase manquante, de double trait union et ne doit pas être vide.", "danger")

                # On doit afficher à nouveau le formulaire "mail_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "mail_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_mail': 13, 'adresse_mail': 'philosophique'}]
                valeur_edit_list = [{'id_mail': id_mail_edit, 'adresse_mail': adresse_mail}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "mail_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('mail/mail_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_mail": id_mail_edit, "value_adresse_mail": adresse_mail}

                # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_mail = Gestionmail()

                # La commande MySql est envoyée à la BD
                data_id_mail = obj_actions_mail.update_mail_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataidmail ", data_id_mail, "type ", type(data_id_mail))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Valeur mail modifiée. ", "success")
                # On affiche les mail avec celui qui vient d'être edité en tête de liste. (DESC)
                return redirect(url_for('mail_afficher', order_by="ASC", id_mail_sel=id_mail_edit))

        except (Exception,
                # pymysql.err.OperationalError,
                # pymysql.ProgrammingError,
                # pymysql.InternalError,
                # pymysql.IntegrityError,
                TypeError) as erreur:
            print(erreur.args[0])
            flash(f"problème mail ____lllupdate{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_adresse_mail_html" alors on renvoie le formulaire "EDIT"
    return render_template('mail/mail_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /mail_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un mail de mail par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/mail_select_delete', methods=['POST', 'GET'])
def mail_select_delete ():
    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = Gestionmail()
            # OM 2019.04.04 Récupère la valeur de "idmailDeleteHTML" du formulaire html "mailDelete.html"
            id_mail_delete = request.args.get('id_mail_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_mail": id_mail_delete}

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_mail = obj_actions_mail.delete_select_mail_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur mail_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur mail_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('mail/mail_delete.html', data=data_id_mail)


# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /mailUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un mail, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/mail_delete', methods=['POST', 'GET'])
def mail_delete ():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = Gestionmail()
            # OM 2019.04.02 Récupère la valeur de "id_mail" du formulaire html "mailAfficher.html"
            id_mail_delete = request.form['id_mail_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_mail": id_mail_delete}

            data_mail = obj_actions_mail.delete_mail_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des mail des mail
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les mail
            return redirect(url_for('mail_afficher',order_by="ASC",id_mail_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "mail" de mail qui est associé dans "t_mail_mail".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des mail !', "warning")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce mail est associé à des mail dans la t_mail_mail !!! : {erreur}")
                # Afficher la liste des mail des mail
                return redirect(url_for('mail_afficher', order_by="ASC", id_mail_sel=0))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur mail_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur mail_delete {erreur.args[0], erreur.args[1]}", "danger")

            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('mail/mail_afficher.html', data=data_mail)

