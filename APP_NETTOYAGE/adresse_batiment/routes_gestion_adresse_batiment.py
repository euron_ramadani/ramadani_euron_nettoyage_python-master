# routes_gestion_adresse_batiment.py
# OM 2020.04.16 Gestions des "routes" FLASK pour la table intermédiaire qui associe les adresse et les batiment.

from flask import render_template, request, flash, session
from APP_NETTOYAGE import obj_mon_application
from APP_NETTOYAGE.batiment.data_gestion_batiment import Gestionbatiment
from APP_NETTOYAGE.adresse_batiment.data_gestion_adresse_batiment import Gestionadressebatiment


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /adresse_batiment_afficher_concat
# Récupère la liste de tous les adresse et de tous les batiment associés aux adresse.
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/adresse_batiment_afficher_concat/<int:id_adresse_sel>", methods=['GET', 'POST'])
def adresse_batiment_afficher_concat (id_adresse_sel):
    print("id_adresse_sel ", id_adresse_sel)
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_batiment = Gestionadressebatiment()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionbatiment()
            # Fichier data_gestion_batiment.py
            data_adresse_batiment_afficher_concat = obj_actions_batiment.adresse_batiment_afficher_data_concat(id_adresse_sel)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data batiment", data_adresse_batiment_afficher_concat, "type ", type(data_adresse_batiment_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_adresse_batiment_afficher_concat:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données batiment affichés dans batimentadresse!!", "success")
            else:
                flash(f"""Le adresse demandé n'existe pas. Ou la table "t_adresse_batiment" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("adresse_batiment/adresse_batiment_afficher.html",
                           data=data_adresse_batiment_afficher_concat)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.21 Définition d'une "route" /gf_edit_adresse_batiment_selected
# Récupère la liste de tous les batiment du adresse sélectionné.
# Nécessaire pour afficher tous les "TAGS" des batiment, ainsi l'utilisateur voit les batiment à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_edit_adresse_batiment_selected", methods=['GET', 'POST'])
def gf_edit_adresse_batiment_selected ():
    if request.method == "GET":
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_batiment = Gestionbatiment()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionbatiment()
            # Fichier data_gestion_batiment.py
            # Pour savoir si la table "t_batiment" est vide, ainsi on empêche l’affichage des tags
            # dans le render_template(adresse_batiment_modifier_tags_dropbox.html)
            data_batiment_all = obj_actions_batiment.batiment_afficher_data('ASC', 0)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données de la table intermédiaire.
            obj_actions_batiment = Gestionadressebatiment()

            # OM 2020.04.21 Récupère la valeur de "id_adresse" du formulaire html "adresse_batiment_afficher.html"
            # l'utilisateur clique sur le lien "Modifier batiment de ce adresse" et on récupère la valeur de "id_adresse" grâce à la variable "id_adresse_batiment_edit_html"
            # <a href="{{ url_for('gf_edit_adresse_batiment_selected', id_adresse_batiment_edit_html=row.id_adresse) }}">Modifier les batiment de ce adresse</a>
            id_adresse_batiment_edit = request.values['id_adresse_batiment_edit_html']

            # OM 2020.04.21 Mémorise l'id du adresse dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_adresse_batiment_edit'] = id_adresse_batiment_edit

            # Constitution d'un dictionnaire pour associer l'id du adresse sélectionné avec un nom de variable
            valeur_id_adresse_selected_dictionnaire = {"value_id_adresse_selected": id_adresse_batiment_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la classe Gestionadressebatiment()
            # 1) Sélection du adresse choisi
            # 2) Sélection des batiment "déjà" attribués pour le adresse.
            # 3) Sélection des batiment "pas encore" attribués pour le adresse choisi.
            # Fichier data_gestion_adresse_batiment.py
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "adresse_batiment_afficher_data"
            data_adresse_batiment_selected, data_adresse_batiment_non_attribues, data_adresse_batiment_attribues = \
                obj_actions_batiment.adresse_batiment_afficher_data(valeur_id_adresse_selected_dictionnaire)

            lst_data_adresse_selected = [item['id_adresse'] for item in data_adresse_batiment_selected]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_adresse_selected  ", lst_data_adresse_selected,
                  type(lst_data_adresse_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les batiment qui ne sont pas encore sélectionnés.
            lst_data_adresse_batiment_non_attribues = [item['id_batiment'] for item in data_adresse_batiment_non_attribues]
            session['session_lst_data_adresse_batiment_non_attribues'] = lst_data_adresse_batiment_non_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_adresse_batiment_non_attribues  ", lst_data_adresse_batiment_non_attribues,
                  type(lst_data_adresse_batiment_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les batiment qui sont déjà sélectionnés.
            lst_data_adresse_batiment_old_attribues = [item['id_batiment'] for item in data_adresse_batiment_attribues]
            session['session_lst_data_adresse_batiment_old_attribues'] = lst_data_adresse_batiment_old_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_adresse_batiment_old_attribues  ", lst_data_adresse_batiment_old_attribues,
                  type(lst_data_adresse_batiment_old_attribues))

            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data data_adresse_batiment_selected", data_adresse_batiment_selected, "type ", type(data_adresse_batiment_selected))
            print(" data data_adresse_batiment_non_attribues ", data_adresse_batiment_non_attribues, "type ",
                  type(data_adresse_batiment_non_attribues))
            print(" data_adresse_batiment_attribues ", data_adresse_batiment_attribues, "type ",
                  type(data_adresse_batiment_attribues))

            # Extrait les valeurs contenues dans la table "t_batiment", colonne "nom_batiment"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_batiment
            lst_data_adresse_batiment_non_attribues = [item['nom_batiment'] for item in data_adresse_batiment_non_attribues]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_all_batiment gf_edit_adresse_batiment_selected ", lst_data_adresse_batiment_non_attribues,
                  type(lst_data_adresse_batiment_non_attribues))

            # Différencier les messages si la table est vide.
            if lst_data_adresse_selected == [None]:
                flash(f"""Le adresse demandé n'existe pas. Ou la table "t_adresse_batiment" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données batiment affichées dans batimentadresse!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("adresse_batiment/adresse_batiment_modifier_tags_dropbox.html",
                           data_batiment=data_batiment_all,
                           data_adresse_selected=data_adresse_batiment_selected,
                           data_batiment_attribues=data_adresse_batiment_attribues,
                           data_batiment_non_attribues=data_adresse_batiment_non_attribues)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /gf_update_adresse_batiment_selected
# Récupère la liste de tous les batiment du adresse sélectionné.
# Nécessaire pour afficher tous les "TAGS" des batiment, ainsi l'utilisateur voit les batiment à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_update_adresse_batiment_selected", methods=['GET', 'POST'])
def gf_update_adresse_batiment_selected ():
    if request.method == "POST":
        try:
            # Récupère l'id du adresse sélectionné
            id_adresse_selected = session['session_id_adresse_batiment_edit']
            print("session['session_id_adresse_batiment_edit'] ", session['session_id_adresse_batiment_edit'])

            # Récupère la liste des batiment qui ne sont pas associés au adresse sélectionné.
            old_lst_data_adresse_batiment_non_attribues = session['session_lst_data_adresse_batiment_non_attribues']
            print("old_lst_data_adresse_batiment_non_attribues ", old_lst_data_adresse_batiment_non_attribues)

            # Récupère la liste des batiment qui sont associés au adresse sélectionné.
            old_lst_data_adresse_batiment_attribues = session['session_lst_data_adresse_batiment_old_attribues']
            print("old_lst_data_adresse_batiment_old_attribues ", old_lst_data_adresse_batiment_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme batiment dans le composant "tags-selector-tagselect"
            # dans le fichier "adresse_batiment_modifier_tags_dropbox.html"
            new_lst_str_adresse_batiment = request.form.getlist('name_select_tags')
            print("new_lst_str_adresse_batiment ", new_lst_str_adresse_batiment)

            # OM 2020.04.29 Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_adresse_batiment_old = list(map(int, new_lst_str_adresse_batiment))
            print("new_lst_adresse_batiment ", new_lst_int_adresse_batiment_old, "type new_lst_adresse_batiment ",
                  type(new_lst_int_adresse_batiment_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2020.04.29 Une liste de "id_batiment" qui doivent être effacés de la table intermédiaire "t_adresse_batiment".
            lst_diff_batiment_delete_b = list(
                set(old_lst_data_adresse_batiment_attribues) - set(new_lst_int_adresse_batiment_old))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_batiment_delete_b ", lst_diff_batiment_delete_b)

            # OM 2020.04.29 Une liste de "id_batiment" qui doivent être ajoutés à la BD
            lst_diff_batiment_insert_a = list(
                set(new_lst_int_adresse_batiment_old) - set(old_lst_data_adresse_batiment_attribues))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_batiment_insert_a ", lst_diff_batiment_insert_a)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_batiment = Gestionadressebatiment()

            # Pour le adresse sélectionné, parcourir la liste des batiment à INSÉRER dans la "t_adresse_batiment".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_batiment_ins in lst_diff_batiment_insert_a:
                # Constitution d'un dictionnaire pour associer l'id du adresse sélectionné avec un nom de variable
                # et "id_batiment_ins" (l'id du batiment dans la liste) associé à une variable.
                valeurs_adresse_sel_batiment_sel_dictionnaire = {"value_FK_adresse": id_adresse_selected,
                                                           "value_FK_batiment": id_batiment_ins}
                # Insérer une association entre un(des) batiment(s) et le adresse sélectionner.
                obj_actions_batiment.adresse_batiment_add(valeurs_adresse_sel_batiment_sel_dictionnaire)

            # Pour le adresse sélectionné, parcourir la liste des batiment à EFFACER dans la "t_adresse_batiment".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_batiment_del in lst_diff_batiment_delete_b:
                # Constitution d'un dictionnaire pour associer l'id du adresse sélectionné avec un nom de variable
                # et "id_batiment_del" (l'id du batiment dans la liste) associé à une variable.
                valeurs_adresse_sel_batiment_sel_dictionnaire = {"value_FK_adresse": id_adresse_selected,
                                                           "value_FK_batiment": id_batiment_del}
                # Effacer une association entre un(des) batiment(s) et le adresse sélectionner.
                obj_actions_batiment.adresse_batiment_delete(valeurs_adresse_sel_batiment_sel_dictionnaire)

            # Récupère les données grâce à une requête MySql définie dans la classe Gestionbatiment()
            # Fichier data_gestion_batiment.py
            # Afficher seulement le adresse dont les batiment sont modifiés, ainsi l'utilisateur voit directement
            # les changements qu'il a demandés.
            data_adresse_batiment_afficher_concat = obj_actions_batiment.adresse_batiment_afficher_data_concat(id_adresse_selected)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data batiment", data_adresse_batiment_afficher_concat, "type ", type(data_adresse_batiment_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_adresse_batiment_afficher_concat == None:
                flash(f"""Le adresse demandé n'existe pas. Ou la table "t_adresse_batiment" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données batiment affichées dans batimentadresse!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Après cette mise à jour de la table intermédiaire "t_adresse_batiment",
    # on affiche les adresse et le(urs) batiment(s) associé(s).
    return render_template("adresse_batiment/adresse_batiment_afficher.html",
                           data=data_adresse_batiment_afficher_concat)
