# data_gestion_adresse_batiment.py
# OM 2020.04.22 Permet de gérer (CRUD) les données de la table intermédiaire "t_adresse_batiment"

from flask import flash
from APP_NETTOYAGE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_NETTOYAGE.DATABASE.erreurs import *


class Gestionadressebatiment():
    def __init__ (self):
        try:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print("dans le try de gestions batiment")
            # OM 2020.04.11 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion batiment adresse ...terrible erreur, il faut connecter une base de donnée", "danger")
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur Gestionadressebatiment {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur Gestionadressebatiment ")

    def batiment_afficher_data (self):
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_batiment"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher
            strsql_batiment_afficher = """SELECT id_batiment, nom_batiment FROM t_batiment ORDER BY id_batiment ASC"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_batiment_afficher)
                # Récupère les données de la requête.
                data_batiment = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_batiment ", data_batiment, " Type : ", type(data_batiment))
                # Retourne les données du "SELECT"
                return data_batiment
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def adresse_batiment_afficher_data (self, valeur_id_adresse_selected_dict):
        print("valeur_id_adresse_selected_dict8..", valeur_id_adresse_selected_dict)
        try:

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_batiment"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_adresse_selected = """SELECT id_adresse, adresse, ville, NPA_adresse, GROUP_CONCAT(id_batiment) as batimentadresse FROM t_adresse_batiment AS T1
                                        INNER JOIN t_adresse AS T2 ON T2.id_adresse = T1.FK_adresse
                                        INNER JOIN t_batiment AS T3 ON T3.id_batiment = T1.FK_batiment
                                        WHERE id_adresse = %(value_id_adresse_selected)s"""

            strsql_adresse_batiment_non_attribues = """SELECT id_batiment, nom_batiment FROM t_batiment
                                                    WHERE id_batiment not in(SELECT id_batiment as idbatimentadresse FROM t_adresse_batiment AS T1
                                                    INNER JOIN t_adresse AS T2 ON T2.id_adresse = T1.FK_adresse
                                                    INNER JOIN t_batiment AS T3 ON T3.id_batiment = T1.FK_batiment
                                                    WHERE id_adresse = %(value_id_adresse_selected)s)"""

            strsql_adresse_batiment_attribues = """SELECT id_adresse, id_batiment, nom_batiment FROM t_adresse_batiment AS T1
                                            INNER JOIN t_adresse AS T2 ON T2.id_adresse = T1.FK_adresse
                                            INNER JOIN t_batiment AS T3 ON T3.id_batiment = T1.FK_batiment
                                            WHERE id_adresse = %(value_id_adresse_selected)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_adresse_batiment_non_attribues, valeur_id_adresse_selected_dict)
                # Récupère les données de la requête.
                data_adresse_batiment_non_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("dfad data_adresse_batiment_non_attribues ", data_adresse_batiment_non_attribues, " Type : ",
                      type(data_adresse_batiment_non_attribues))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_adresse_selected, valeur_id_adresse_selected_dict)
                # Récupère les données de la requête.
                data_adresse_selected = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_adresse_selected  ", data_adresse_selected, " Type : ", type(data_adresse_selected))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_adresse_batiment_attribues, valeur_id_adresse_selected_dict)
                # Récupère les données de la requête.
                data_adresse_batiment_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_adresse_batiment_attribues ", data_adresse_batiment_attribues, " Type : ",
                      type(data_adresse_batiment_attribues))

                # Retourne les données du "SELECT"
                return data_adresse_selected, data_adresse_batiment_non_attribues, data_adresse_batiment_attribues
        except pymysql.Error as erreur:
            print(f"DGGF gfad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def adresse_batiment_afficher_data_concat (self, id_adresse_selected):
        print("id_adresse_selected  ", id_adresse_selected)
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_batiment"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_adresse_batiment_afficher_data_concat = """SELECT id_adresse, adresse, ville, NPA_adresse,
                                                            GROUP_CONCAT(nom_batiment) as batimentadresse FROM t_adresse_batiment AS T1
                                                            RIGHT JOIN t_adresse AS T2 ON T2.id_adresse = T1.FK_adresse
                                                            LEFT JOIN t_batiment AS T3 ON T3.id_batiment = T1.FK_batiment
                                                            GROUP BY id_adresse"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # le paramètre 0 permet d'afficher tous les adresse
                # Sinon le paramètre représente la valeur de l'id du adresse
                if id_adresse_selected == 0:
                    mc_afficher.execute(strsql_adresse_batiment_afficher_data_concat)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du adresse sélectionné avec un nom de variable
                    valeur_id_adresse_selected_dictionnaire = {"value_id_adresse_selected": id_adresse_selected}
                    strsql_adresse_batiment_afficher_data_concat += """ HAVING id_adresse= %(value_id_adresse_selected)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_adresse_batiment_afficher_data_concat, valeur_id_adresse_selected_dictionnaire)

                # Récupère les données de la requête.
                data_adresse_batiment_afficher_concat = mc_afficher.fetchall()
                # Affichage dans la console
                print("dggf data_adresse_batiment_afficher_concat ", data_adresse_batiment_afficher_concat, " Type : ",
                      type(data_adresse_batiment_afficher_concat))

                # Retourne les données du "SELECT"
                return data_adresse_batiment_afficher_concat


        except pymysql.Error as erreur:
            print(f"DGGF gfadc pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfadc Exception {erreur.args}")
            raise MaBdErreurConnexion(
                f"DGG gfadc Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfadc pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def adresse_batiment_add (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_adresse" et "id_batiment" dans la "t_adresse_batiment"
            strsql_insert_adresse_batiment = """INSERT INTO t_adresse_batiment (id_adresse_batiment, FK_batiment, FK_adresse)
                                            VALUES (NULL, %(value_FK_batiment)s, %(value_FK_adresse)s)"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_adresse_batiment, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

    def adresse_batiment_delete (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Effacer une (des) association(s) existantes entre "id_adresse" et "id_batiment" dans la "t_adresse_batiment"
            strsql_delete_adresse_batiment = """DELETE FROM t_adresse_batiment WHERE FK_batiment = %(value_FK_batiment)s AND FK_adresse = %(value_FK_adresse)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_delete_adresse_batiment, valeurs_insertion_dictionnaire)
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème adresse_batiment_delete Gestions batiment adresse numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème adresse_batiment_delete Gestions batiment adresse  numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème adresse_batiment_delete Gestions batiment adresse  {erreur}")

    def edit_batiment_data (self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le batiment sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_adresses = "SELECT id_batiment, nom_batiment FROM t_batiment WHERE id_batiment = %(value_id_batiment)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_adresses, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_batiment_data Data Gestions batiment numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions batiment numéro de l'erreur : {erreur}", "danger")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_batiment_data d'un batiment Data Gestions batiment {erreur}")

    def update_batiment_data (self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditnombatimentHTML" du form HTML "batimentEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnombatimentHTML" value="{{ row.nom_batiment }}"/></td>
            str_sql_update_nombatiment = "UPDATE t_batiment SET nom_batiment = %(value_name_genre)s WHERE id_batiment = %(value_id_batiment)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_nombatiment, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_batiment_data Data Gestions batiment numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions batiment numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_batiment_data d\'un batiment Data Gestions batiment {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "warning")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash(f"Doublon !!! Introduire une valeur différente", "warning")
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_batiment_data Data Gestions batiment numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_batiment_data d'un batiment DataGestionsbatiment {erreur}")

    def delete_select_batiment_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditnombatimentHTML" du form HTML "batimentEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnombatimentHTML" value="{{ row.nom_batiment }}"/></td>

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le batiment sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_batiment = "SELECT id_batiment, nom_batiment FROM t_batiment WHERE id_batiment = %(value_id_batiment)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode"mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_batiment, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_select_batiment_data Gestions batiment numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_batiment_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_batiment_data d\'un batiment Data Gestions batiment {erreur}")

    def delete_batiment_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "batimentEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditnombatimentHTML" value="{{ row.nom_batiment }}"/></td>
            str_sql_delete_nombatiment = "DELETE FROM t_batiment WHERE id_batiment = %(value_id_batiment)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_nombatiment, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_batiment_data Data Gestions batiment numéro de l'erreur : {erreur}")
            flash(f"Flash. Problèmes Data Gestions batiment numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # OM 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un batiment qui est associé à un adresse dans la table intermédiaire "t_adresse_batiment"
                # il y a une contrainte sur les FK de la table intermédiaire "t_adresse_batiment"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce batiment est associé à des adresse dans la t_adresse_batiment !!! : {erreur}", "danger")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !!! Ce batiment est associé à des adresse dans la t_adresse_batiment !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")
